package com.radoslav.socialreminder;

import java.util.ArrayList;

/**
 * Created by Rado on 08/09/2015.
 */
public class userDetails {

    public static int loggedUserID = 0;
    public static int currentGroupChosenID = 0;
    public static boolean userLoggedIn = false;
    public static String loggedUserName = "";
    public static String loggedEmail = "";


    public static int groupChosenOrder = 0;


    public static ArrayList<String> groups = new ArrayList<>();
    public static ArrayList<Integer> addTasksPermission = new ArrayList<>();
    public static ArrayList<Integer> manageTagsPermission = new ArrayList<>();
    public static ArrayList<Integer> manageGroupUsersPermission = new ArrayList<>();

    public userDetails() {

    }


    public static String getLoggedEmail() {
        return loggedEmail;
    }
    public static void setLoggedEmail(String email) {loggedEmail = email; }




    public String getLoggedUserName() {
        return loggedUserName;
    }
    public void setLoggedUserName(String string) {
        loggedUserName = string;
    }




    public void setLoggedUserID(int num) {
            loggedUserID = num;
        }
    public int getLoggedUserID() {
        return loggedUserID;
    }




    public void setCurrentGroupChosenID(int num) {
            currentGroupChosenID = num;
        }
    public int getCurrentGroupChosenID() {
        return currentGroupChosenID;
    }



        public void setUserLoggedIn(Boolean bool)
        {
            userLoggedIn = bool;
        }
        public boolean getUserLoggedIn()
        {
            return userLoggedIn;
        }





    public static ArrayList<String> getGroups() {
        return groups;
    }

    public static void setGroups(ArrayList<String> groups) {
        userDetails.groups = groups;
    }

    public static ArrayList<Integer> getAddTasksPermission() {
        return addTasksPermission;
    }

    public static void setAddTasksPermission(ArrayList<Integer> addTasksPermission) {
        userDetails.addTasksPermission = addTasksPermission;
    }

    public static ArrayList<Integer> getManageTagsPermission() {
        return manageTagsPermission;
    }

    public static void setManageTagsPermission(ArrayList<Integer> manageTagsPermission) {
        userDetails.manageTagsPermission = manageTagsPermission;
    }

    public static ArrayList<Integer> getManageGroupUsersPermission() {
        return manageGroupUsersPermission;
    }

    public static void setManageGroupUsersPermission(ArrayList<Integer> manageGroupUsersPermission) {
        userDetails.manageGroupUsersPermission = manageGroupUsersPermission;
    }



    public static int getGroupChosenOrder() {
        return groupChosenOrder;
    }

    public static void setGroupChosenOrder(int groupChosenOrder) {
        userDetails.groupChosenOrder = groupChosenOrder;
    }

}
