package com.radoslav.socialreminder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class allUsersPermissions extends AppCompatActivity {
    private Toolbar mToolbar;

    private ListView lv;

    private ProgressDialog pDialog;
    JSONParser jParser = new JSONParser();

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERS = "users";

    private JSONArray products = null;
    private static final String url_all_users = "http://radoslavbonev.net/get_all_users.php";
    private static final String url_get_names = "http://radoslavbonev.net/get_user_names.php";

    private ArrayList<String>  IDs = new ArrayList<>();
    private ArrayList<String>  groupUsers = new ArrayList<>();
    private ArrayList<String>  names = new ArrayList<>();
    private ArrayList<String>  addTasksPermission = new ArrayList<>();
    private ArrayList<String>  manageTagsPermission = new ArrayList<>();
    private ArrayList<String>  manageGroupUsersPermission = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_users_permissions);


        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        new LoadAllUsers().execute();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_all_users_permissions, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if((requestCode == 6) && resultCode == 666)
        {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage(data.getStringExtra("message"));
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            recreate();
                        }
                    });
            AlertDialog showTheAlert = dialogBuilder.create();
            showTheAlert.show();
        }
    }

    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(allUsersPermissions activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return IDs.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item_users, null);
            }

            // Initialize the views in the layout
         //   ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView tvIDs = (TextView) listItem.findViewById(R.id.id);
            TextView tvUsername = (TextView) listItem.findViewById(R.id.userName);
            TextView tvPermission = (TextView) listItem.findViewById(R.id.permission);


            //  iv.setBackgroundResource(thumb[pos]);  // **************
            tvIDs.setText(IDs.get(pos));
            tvUsername.setText(names.get(pos));
            tvPermission.setText("Add tasks? " + addTasksPermission.get(pos) + "   Manage Tags? " + manageTagsPermission.get(pos) + "   Manage people? " + manageGroupUsersPermission.get(pos));

            return listItem;
        }

    }





    class LoadAllUsers extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(allUsersPermissions.this);
            pDialog.setMessage(getString(R.string.loading_tasks_group));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));
            JSONObject json = jParser.makeHttpRequest(url_all_users, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    products = json.getJSONArray(TAG_USERS);


                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        String id = c.getString("id");
                        IDs.add(id);


                        String user = c.getString("user");
                        groupUsers.add(user);


                        String addTasks = c.getString("add_tasks");
                        addTasksPermission.add(addTasks);


                        String manageTags = c.getString("manage_tags");
                        manageTagsPermission.add(manageTags);


                        String manageGroupUsers = c.getString("manage_group_users");
                        manageGroupUsersPermission.add(manageGroupUsers);


                    }





                } else {
                    Intent i = new Intent(getApplicationContext(),
                            myTasks.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            new LoadAllUserNames().execute();

        }

    }







    class LoadAllUserNames extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(allUsersPermissions.this);
            pDialog.setMessage(getString(R.string.loading_groups));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jParser.makeHttpRequest(url_get_names, "GET", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                System.out.println("success:   " + success);

                if (success == 1) {

                   JSONArray users = json.getJSONArray("users");

                    for (int i = 0; i < users.length(); i++) {
                        JSONObject c = users.getJSONObject(i);

                        int id = Integer.parseInt(c.getString("pid"));
                       for(int d = 0; d<IDs.size(); d++)
                       {
                           if(Integer.parseInt(groupUsers.get(d)) == id)
                           {

                               names.add(c.getString("name"));
                               System.out.println("n " + c.getString("name"));
                           }
                           System.out.println("----------------");
                       }

                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            lv = (ListView) findViewById(R.id.listView);
            lv.setAdapter(new VersionAdapter(allUsersPermissions.this));
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                        long arg3) {
                    // TODO Auto-generated method stub


                    LayoutInflater layoutInflator = getLayoutInflater();


                    int currentID = Integer.parseInt(((TextView) arg1.findViewById(R.id.id)).getText()
                            .toString());
                    System.out.println(currentID);
                    Intent intent = new Intent(allUsersPermissions.this, editGroupUser.class);
                    intent.putExtra("id", currentID);
                    intent.putExtra("userName", names.get(arg2));
                    System.out.println("nameeee: " + names.get(arg2));
                    startActivityForResult(intent, 6);

                }
            });

        }

    }


}
