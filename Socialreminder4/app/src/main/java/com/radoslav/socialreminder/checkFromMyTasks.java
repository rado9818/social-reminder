package com.radoslav.socialreminder;

import java.util.ArrayList;

/**
 * Created by Rado on 11/07/2015.
 */

// This class generates the chance for a task out of the user tasks to match one of the group's tasks

public class checkFromMyTasks {

    private ArrayList<String> myTasks = new ArrayList<String>();
    private ArrayList<String> currentGroupTasks = new ArrayList<String>();
    String[] parts;

    private String taskTags = null;
    private double numMatches = 0;
    private double chanceOfMatching = 0;

    private String assignedDay = null;
    private String deadline = null;

    private String assignedDayGroupTask[] = null;
    private String deadlineGroupTask[] = null;

    private int minChanceToAdd = 80;  //indicates the minimum chance that tasks should
    // have in order to be added to the group DB

    private int preciseRate = 10; // represents how the chanceOfMatching double will increase or
    // reduce because of the assignedDays and deadlines matching (or not matching)

    private ArrayList<String> processToAdding = new ArrayList<String>();

    private String ExternalTaskTags[];


    private int provider = 0; // int provider takes:
    // @ 0 - means we will compare a group task out of the local task (perform a sync)
    // @ 1 - means we are uploading a task to the group's DB if it does not exist
         // (it is uploaded to the local DB as well



 //   private int loop = 0;

  //  private int i = 0;


    public checkFromMyTasks(int provider, String taskTags, String assignedDay, String deadline,
                            String anotherTags, String anotherAssignedDay, String anotherDeadline)
    {
        this.taskTags = taskTags;

        this.assignedDay = assignedDay;
        this.deadline = deadline;

        this.provider = provider;


        if(provider == 0)
        {
           // tasksLocalDB = new tasksLocalDB();
            ExternalTaskTags = anotherTags.split(", ");
            assignedDayGroupTask = new String[]{assignedDay};
            deadlineGroupTask = new String[]{deadline};
        }

        else if(provider == 1) {

            System.out.println("array:   " + anotherTags);

            ExternalTaskTags = anotherTags.split(", ");



            assignedDayGroupTask = new String[] {anotherAssignedDay};
            deadlineGroupTask = new String[] {anotherDeadline};
        }



        parseTags();

        initialiseCurrentLocalTask();


            //TODO: create a loop which initialises all the task's tags
            initialiseCurrentGroupTasks();

            compare();

            calculatePercentage();

            precisePercentage();

            addTaskToGroupDB();



    }

    private void parseTags()
    {
        parts = taskTags.split(", ");

    }

    private void initialiseCurrentLocalTask()
    {
        for(int i = 0; i < parts.length; i++) {
            myTasks.add(parts[i]);
            System.out.println("to comp:   " + myTasks.get(i));

        }
    }


    private void initialiseCurrentGroupTasks()
    {
        // TODO: replace the 1 with an actual counter through all the ExternalTaskTags tasks


        for(int i = 0; i < ExternalTaskTags.length; i++) {
                currentGroupTasks.add(ExternalTaskTags[i]);
            System.out.println("to compare:   " + ExternalTaskTags[i]);

        }



    }

    private void clearCurrentGroupTasks()
    {
        currentGroupTasks.clear();
        assignedDayGroupTask = null;
        deadlineGroupTask = null;
    }

    private void compare()
    {
        for(int mt = 0; mt < myTasks.size(); mt++)
        {
            for(int gt = 0; gt < currentGroupTasks.size(); gt++)
            {
                if(myTasks.get(mt).equals(currentGroupTasks.get(gt)))
                {
                    numMatches++;
                }
            }
        }
    }

    private void calculatePercentage()
    {

        chanceOfMatching = (numMatches/myTasks.size()) * 100;

        System.out.println("first:   " + chanceOfMatching);
    }

    private void precisePercentage()
    {
        //TODO: change the zero with an actual value
        String assignedDayGroupTask = this.assignedDayGroupTask[0];
        String deadlineGroupTask = this.deadlineGroupTask[0];

        if(assignedDayGroupTask != null && assignedDay != null)
        {
            if(assignedDayGroupTask.equals(assignedDay))
            {
                if(chanceOfMatching + preciseRate <= 100)
                {
                    chanceOfMatching += preciseRate;
                }
            }

            else
            {
                if(chanceOfMatching - preciseRate >= 0)
                {
                    chanceOfMatching -= preciseRate;
                }
            }

        }

        if(deadlineGroupTask != null && deadline != null)
        {
            if(deadlineGroupTask.equals(deadline))
            {
                if(chanceOfMatching + preciseRate <= 100)
                {
                    chanceOfMatching += preciseRate;
                }
            }
            else
            {
                if(chanceOfMatching - preciseRate >= 0)
                {
                    chanceOfMatching -= preciseRate;
                }
            }
        }

        System.out.println("The chance of matching is: " + chanceOfMatching);

    }
    private void addTaskToGroupDB()
    {
        if(chanceOfMatching < minChanceToAdd)
        {
            processToAdding.add("true");
        }
        else
        {
            processToAdding.add("false");
            /* TODO: call a function which sets the task  name, tags, assignedDay, deadline(the one we
            doubt whether it exists in the groupDB). A popup asks the user whether the tasks are the same and
            adds the answer into the array in order to improve the preciseness*/
        }
    }

    public ArrayList<String> getProcessToAdding()
    {
        return processToAdding;
    }


    public double getChanceOfMatching ()
    {
        return chanceOfMatching;
    }


}