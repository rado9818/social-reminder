package com.radoslav.socialreminder;

/**
 * Created by Rado on 14/07/2015.
 */

//this class get the tasks out of the local DB and supplies the other classes with them

public class tasksLocalDB
{

     String[] taskName, taskTags, assignedDay, deadline;


    public tasksLocalDB()
    {

        //TODO: retrieve the properties from an actual DB
        taskName = new String[]{"title"};
        taskTags = new String[]{"Maths, test", "BL, essay", "Physics, protocol"};
        assignedDay = new String[]{"14/07/1998"};
        deadline = new String[]{"14/07/1998"};
    }


    public String[] getTaskName()
    {
        return taskName;
    }

    public String[] getTaskTags() {
        return taskTags;
    }

    public String[] getAssignedDay() {
        return assignedDay;
    }

    public String[] getDeadline() {
        return deadline;
    }
}
