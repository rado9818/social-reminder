package com.radoslav.socialreminder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class editGroupUser extends AppCompatActivity implements View.OnClickListener,
        OnShowcaseEventListener, AdapterView.OnItemClickListener{
    private Toolbar mToolbar;

    private ToggleButton addTasks, manageTags, manageGroupUsers;
    private Button saveButton, deleteUser;

    private int id;
    private ProgressDialog pDialog;
    private JSONParser jsonParser = new JSONParser();

    private static final String TAG_SUCCESS = "success";
    private static final String url_get_permission = "http://radoslavbonev.net/get_user_permission.php";
    private static final String url_update_permission = "http://radoslavbonev.net/update_permission.php";
    private static final String url_delete_user = "http://radoslavbonev.net/delete_user.php";

    private String addTasksEnabled, manageTagsEnabled, manageGroupUsersEnabled;

    private TextView addTasksText, manageTagsText, manageGroupUsersText;

    private String userName;
    TextView user;


    final String PREFS_NAME = "editGroupUserTip";
    final String PREF_VERSION_CODE_KEY = "version_code";
    final int DOESNT_EXIST = -1;

    private String name;
    ShowcaseView sv;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_group_user);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        HardcodedListAdapter adapter = new HardcodedListAdapter(this);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);


        user = (TextView) findViewById(R.id.user);

        addTasks = (ToggleButton) findViewById(R.id.addTasksToggle);
        manageTags = (ToggleButton) findViewById(R.id.manageTagsToggle);
        manageGroupUsers = (ToggleButton) findViewById(R.id.manageGroupUsersToggle);

        addTasksText = (TextView) findViewById(R.id.addTasksText);
        manageTagsText = (TextView) findViewById(R.id.manageTagsText);
        manageGroupUsersText = (TextView) findViewById(R.id.manageUsersText);


        addTasksText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(editGroupUser.this);
                dialogBuilder.setTitle(R.string.what_is_this_about);
                dialogBuilder.setMessage(R.string.add_tasks_permission_explained);
                dialogBuilder.setCancelable(true);
                dialogBuilder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });


                AlertDialog showTheAlert = dialogBuilder.create();
                showTheAlert.show();
                TextView messageView = (TextView)showTheAlert.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }
        });

        manageTagsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(editGroupUser.this);
                dialogBuilder.setTitle(R.string.what_is_this_about);
                dialogBuilder.setMessage(R.string.manage_tags_explained);
                dialogBuilder.setCancelable(true);
                dialogBuilder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });


                AlertDialog showTheAlert = dialogBuilder.create();
                showTheAlert.show();
                TextView messageView = (TextView)showTheAlert.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }
        });

        manageGroupUsersText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(editGroupUser.this);
                dialogBuilder.setTitle(R.string.what_is_this_about);
                dialogBuilder.setMessage(R.string.manage_users_exlained);
                dialogBuilder.setCancelable(true);
                dialogBuilder.setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });


                AlertDialog showTheAlert = dialogBuilder.create();
                showTheAlert.show();
                TextView messageView = (TextView)showTheAlert.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }
        });

        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);


        userName = intent.getStringExtra("userName");
        System.out.println("id: " + id);


        user.append(userName);
        new getPermission().execute();

        saveButton = (Button) findViewById(R.id.btnSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addTasks.isChecked()) {
                    addTasksEnabled = "1";
                }
                else {
                    addTasksEnabled = "0";
                }

                if(manageTags.isChecked()) {
                    manageTagsEnabled = "1";
                }
                else {
                    manageTagsEnabled = "0";
                }

                if(manageGroupUsers.isChecked())
                {
                    manageGroupUsersEnabled = "1";
                }
                else
                {
                    manageGroupUsersEnabled = "0";
                }
                new savePermission().execute();
            }
        });
        deleteUser = (Button) findViewById(R.id.deleteUser);
        deleteUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder buildDialog = new AlertDialog.Builder(editGroupUser.this);
                buildDialog.setMessage(getString(R.string.do_you_wanna_delete) + userName);
                buildDialog.setCancelable(true);
                buildDialog.setPositiveButton(R.string.yes,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                new deleteUser().execute();
                                dialog.dismiss();
                            }
                        });

                buildDialog.setNegativeButton(R.string.decide_later,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });


                AlertDialog showTheDialog = buildDialog.create();
                showTheDialog.show();
                TextView messageView = (TextView)showTheDialog.findViewById(android.R.id.message);
                messageView.setGravity(Gravity.CENTER);
            }
        });





        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        //I would be able to easily check whether it is a normal run, a new version is installed, or this is the first run
        //if I need
        if (savedVersionCode == DOESNT_EXIST) {
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);



            ViewTarget target = new ViewTarget(R.id.addTasksText, this);
            sv = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(target)
                    .setContentTitle(getString(R.string.you_have_the_control))
                    .setContentText(getString(R.string.editGroupUser_user_guide))
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .setShowcaseEventListener(this)
                    .build();
            sv.setButtonPosition(lps);

        }
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_group_user, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





    class getPermission extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editGroupUser.this);
            pDialog.setMessage(getString(R.string.retreiving_permssion));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {

                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("id", String.valueOf(id)));


                        JSONObject json = jsonParser.makeHttpRequest(
                                url_get_permission, "GET", params);


                        success = json.getInt(TAG_SUCCESS);

                        if (success == 1) {

                            JSONArray productObj = json
                                    .getJSONArray("permission");

                            JSONObject product = productObj.getJSONObject(0);

                            System.out.println(Integer.parseInt(product.getString("add_tasks")));
                            System.out.println(Integer.parseInt(product.getString("manage_tags")));
                            System.out.println(Integer.parseInt(product.getString("manage_group_users")));

                            if(Integer.parseInt(product.getString("add_tasks")) == 1)
                            {
                                addTasks.setChecked(true);
                            }

                            if(Integer.parseInt(product.getString("manage_tags")) == 1)
                            {
                                manageTags.setChecked(true);
                            }
                            if(Integer.parseInt(product.getString("manage_group_users")) == 1)
                            {
                                manageGroupUsers.setChecked(true);
                            }

                        }else{

                        }
                    } catch (JSONException e) {
                        System.out.println("E " + e);
                    }
                }
            });

            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }






    class savePermission extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editGroupUser.this);
            pDialog.setMessage(getString(R.string.saving_permission));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {




            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("add_tasks", addTasksEnabled));
            params.add(new BasicNameValuePair("manage_tags", manageTagsEnabled));
            params.add(new BasicNameValuePair("manage_group_users", manageGroupUsersEnabled));
            params.add(new BasicNameValuePair("id", String.valueOf(id)));


            JSONObject json = jsonParser.makeHttpRequest(url_update_permission,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent intent = getIntent();
                    intent.putExtra("message", getString(R.string.you_set) + userName + getString(R.string.sb_permission_to) +
                            getString(R.string.add_tags_permission) + addTasks.isChecked() + "\n" +
                            getString(R.string.manage_tags_permission) + manageTags.isChecked() + "\n" +
                            getString(R.string.manage_people_permission) + manageGroupUsers.isChecked());
                    setResult(666, intent);
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            finish();
        }
    }



    class deleteUser extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editGroupUser.this);
            pDialog.setMessage(getString(R.string.deleting_the_user));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            int success;
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("id", String.valueOf(id)));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_delete_user, "POST", params);


                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {

                    Intent intent = getIntent();
                    intent.putExtra("message", getString(R.string.successfully_deleted_user) + userName);
                    setResult(666, intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            finish();


        }

    }






    @Override
    public void onClick(View view) {

        int viewId = view.getId();
        switch (viewId) {
            case R.id.addTasksText:
                if (sv.isShown()) {
                    sv.setStyle(R.style.CustomShowcaseTheme);
                } else {
                    sv.show();
                }
                break;
        }
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            listView.setAlpha(1f);
        }
    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

    }


    private static class HardcodedListAdapter extends ArrayAdapter {


        public HardcodedListAdapter(Context context) {
            super(context, R.layout.activity_edit_group_user);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_edit_group_user, parent, false);
            }

            return convertView;
        }
    }

}
