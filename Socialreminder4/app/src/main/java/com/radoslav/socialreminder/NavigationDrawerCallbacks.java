package com.radoslav.socialreminder;

/**
 * Created by Rado on 12/08/2015.
 */
public interface navigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}