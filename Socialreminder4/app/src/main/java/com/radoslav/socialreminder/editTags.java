package com.radoslav.socialreminder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class editTags extends AppCompatActivity {

    EditText txtTags;
    EditText txtDesc;
    EditText txtCreatedAt;
    Button btnSave;
    Button btnDelete;

    String pid;

    private Toolbar mToolbar;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_tags_details = "http://radoslavbonev.net/get_tags_details.php";

    private static final String url_update_tag = "http://radoslavbonev.net/update_tag.php";

    private static final String url_delete_tag = "http://radoslavbonev.net/delete_tag.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCT = "product";


    private String oldTag = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_tags_details);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        txtTags = (EditText) findViewById(R.id.tagsText);

         Intent intent = getIntent();

        pid = intent.getStringExtra("id");

        new GetTagDetails().execute();

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                txtTags.setError(null);

                if(tagsValid()) {
                    if(tagsNotTooLong())
                    {
                new SaveTag().execute();
                    }
                    else
                    {
                        txtTags.setError(getString(R.string.tags_too_many_chars));
                        Toast.makeText(getApplicationContext(), getString(R.string.tags_too_many_chars), Toast.LENGTH_LONG).show();
                    }
                }

                else{
                    txtTags.setError(getString(R.string.tags_not_valid));
                    Toast.makeText(getApplicationContext(), getString(R.string.tags_not_valid), Toast.LENGTH_LONG).show();
                }


            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                new DeleteTag().execute();
            }
        });

    }
    private boolean tagsValid()
    {
        String tags = txtTags.getText().toString();
        String[] singleTags = tags.split(", ");
        return singleTags.length > 1;
    }
    private boolean tagsNotTooLong()
    {
        String tags = txtTags.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<100 && chars>4;
    }

    class GetTagDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editTags.this);
            pDialog.setMessage(getString(R.string.loading_the_tag));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {

                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("id", pid));


                        JSONObject json = jsonParser.makeHttpRequest(
                                url_tags_details, "GET", params);


                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {

                            JSONArray productObj = json
                                    .getJSONArray(TAG_PRODUCT);

                            JSONObject product = productObj.getJSONObject(0);


                            txtTags.setText(product.getString("tags_group"));
                            oldTag = product.getString("tags_group");

                        }else{

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }


    class SaveTag extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editTags.this);
            pDialog.setMessage(getString(R.string.saving_the_tag));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {


            String tags = txtTags.getText().toString();


                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("tags_id", pid));
                params.add(new BasicNameValuePair("tags_group", tags));


                JSONObject json = jsonParser.makeHttpRequest(url_update_tag,
                        "POST", params);

                try {
                    int success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        Intent i = getIntent();
                        i.putExtra("newTag", tags);
                        i.putExtra("oldTag", oldTag);
                        setResult(103, i);
                        finish();
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();


        }
    }


    class DeleteTag extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editTags.this);
            pDialog.setMessage(getString(R.string.deleting_tag));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            int success;
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("tags_id", pid));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_delete_tag, "POST", params);

                Log.d("Delete User", json.toString());

                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {

                    Intent i = getIntent();

                    i.putExtra("oldTag", oldTag);
                    setResult(106, i);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();

        }

    }
}
