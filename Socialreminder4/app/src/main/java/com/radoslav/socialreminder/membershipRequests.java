package com.radoslav.socialreminder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class membershipRequests extends AppCompatActivity{

    private Toolbar mToolbar;



    JSONParser jParser = new JSONParser();


    private JSONArray products = null;


    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_requests = "http://radoslavbonev.net/get_all_requests.php";
    private static final String url_choose_group = "http://radoslavbonev.net/choose_group.php";
    private static final String url_delete_request = "http://radoslavbonev.net/delete_request.php";
    private static final String url_load_groups = "http://radoslavbonev.net/get_user_groups_permission.php";


    performSync performSync;


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERS = "users";


    private ArrayList<String> taskTags = new ArrayList<>();
    private ArrayList<String> forGroups = new ArrayList<>();
    private ArrayList<String> userNames = new ArrayList<>();
    private ArrayList<String> userIDs = new ArrayList<>();
    private ArrayList<String> requestId = new ArrayList<>();
    private ArrayList<String> taskAssignedDays = new ArrayList<>();
    private ArrayList<String> taskDeadlines = new ArrayList<>();

    private ListView lv;

    private String stringRequestID = "", stringUserID = "", stringUserName = "", stringDesiredGroup = "";

    private int numberOfUserGroups = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_membership_requests);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        new LoadAllTasks().execute();


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group_tasks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class LoadAllTasks extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
                pDialog = new ProgressDialog(membershipRequests.this);
                pDialog.setMessage(getString(R.string.loading_requests));
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();

        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            System.out.println("group:   " + String.valueOf(userDetails.currentGroupChosenID));
            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));


            JSONObject json = jParser.makeHttpRequest(url_requests, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);


                System.out.println("success:   " + success);

                if (success == 1) {

                    products = json.getJSONArray(TAG_USERS);

                    System.out.println("products heap:   " + products);

                    for (int i = 0; i < products.length(); i++) {
                        System.out.println("products loop:   " + i);
                        JSONObject c = products.getJSONObject(i);

                        String id = c.getString("request_id");
                        requestId.add(id);
                        System.out.println("id:   " + id);

                        String for_group = c.getString("for_group");
                        forGroups.add(for_group);
                        System.out.println("for_group:   " + for_group);

                        String user_id = c.getString("user_id");
                        userIDs.add(user_id);
                        System.out.println("user_id:   " + user_id);


                        String user_name = c.getString("user_name");
                        userNames.add(user_name);
                        System.out.println("user_name:   " + user_name);


                    }
                } else {

                    Intent i = new Intent(getApplicationContext(),
                            newGroup.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    lv = (ListView) findViewById(R.id.listView);
                    lv.setAdapter(new VersionAdapter(membershipRequests.this));
                    lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
                    lv.setDividerHeight(6);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                long arg3) {
                            // TODO Auto-generated method stub

                            int pos = arg2;

                            LayoutInflater layoutInflator = getLayoutInflater();

                            View layout = layoutInflator.inflate(R.layout.custom_toast,
                                    (ViewGroup) findViewById(R.id.toast_layout_root));


                            // ImageView iv = (ImageView) layout.findViewById(R.id.toast_iv);
                            TextView tv = (TextView) layout.findViewById(R.id.toast_tv);

                            // iv.setBackgroundResource(thumb[pos]); //****************************

                            stringRequestID = requestId.get(pos);
                            stringUserID = userIDs.get(pos);
                            stringUserName = userNames.get(pos);
                            stringDesiredGroup = forGroups.get(pos);

                            new LoadGroups().execute();

                            AlertDialog.Builder buildDialog = new AlertDialog.Builder(membershipRequests.this);
                            buildDialog.setMessage("Do you want to accept " + stringUserName + "'s join request? \n To dismiss the dialogue press the back button and come back when you decide what to do.");
                            buildDialog.setCancelable(true);
                            buildDialog.setPositiveButton("Accept",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            if ((numberOfUserGroups < 3) && (numberOfUserGroups != -1))

                                            {
                                                new SaveGroup().execute();
                                                dialog.dismiss();
                                                numberOfUserGroups = -1;
                                            } else {
                                                Toast.makeText(getApplicationContext(), R.string.number_of_groups_restriction, Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });

                            buildDialog.setNegativeButton("Delete the request",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            new DeleteRequest().execute();
                                            dialog.dismiss();
                                        }
                                    });


                            AlertDialog showTheDialog = buildDialog.create();
                            showTheDialog.show();
                            TextView messageView = (TextView)showTheDialog.findViewById(android.R.id.message);
                            messageView.setGravity(Gravity.CENTER);
                        }
                    });

                }
            });

        }


    }


    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(membershipRequests activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return userNames.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item_membership, null);
            }

         //   ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView tvID = (TextView) listItem.findViewById(R.id.pid);
            TextView tvDesc = (TextView) listItem.findViewById(R.id.personName);
            //   TextView tvTags = (TextView) listItem.findViewById(R.id.tagsText);

            //  iv.setBackgroundResource(thumb[pos]);  // **************
         //   tvTitle.setText(taskTags.get(pos));
            tvID.setText(userIDs.get(pos));
            System.out.println("id to set:   " + userIDs.get(pos));
            tvDesc.setText(userNames.get(pos));
            System.out.println("name to set:   " + userNames.get(pos));

            //   tvTags.setText(internalTaskTags.get(pos));

            return listItem;
        }

    }






    //-------------add to a group------------------------------------------\\



    class SaveGroup extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(membershipRequests.this);
            pDialog.setMessage(getString(R.string.adding_user));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("user_id", stringUserID));
            params.add(new BasicNameValuePair("group_id", stringDesiredGroup));



            JSONObject json = jParser.makeHttpRequest(url_choose_group,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = getIntent();
                    setResult(100, i);
                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    new DeleteRequest().execute();
                }
            });
        }
    }








    class DeleteRequest extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
                pDialog = new ProgressDialog(membershipRequests.this);
                pDialog.setMessage(getString(R.string.deleting_the_request));
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(false);
                pDialog.show();
        }


        protected String doInBackground(String... args) {

            int success;
            try {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("request_id", stringRequestID));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_delete_request, "POST", params);

                Log.d("Delete User", json.toString());

                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {




                }
            } catch (JSONException e) {
                System.out.println("Ex " + e);
            }

            return null;
        }

        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            recreate();

        }

    }














    class LoadGroups extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(membershipRequests.this);
            pDialog.setMessage(getString(R.string.checking_if_user_can_join));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            numberOfUserGroups = -1;
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();


            params.add(new BasicNameValuePair("user", stringUserID));
            JSONObject json = jsonParser.makeHttpRequest(url_load_groups, "GET", params);



            try {
                int success = 0;
                success = json.getInt(TAG_SUCCESS);


                System.out.println("success " + success);
                if (success == 1) {


                    JSONArray groups = json.getJSONArray("groups");

                    numberOfUserGroups += 1;

                    for (int i = 0; i < groups.length(); i++) {


                        numberOfUserGroups += 1;

                    }



                } else{

                    numberOfUserGroups = 0;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {


                    System.out.println("num: " + numberOfUserGroups);
                }
            });

        }

    }



}
