package com.radoslav.socialreminder;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class allGroups extends ListActivity implements View.OnClickListener,
        OnShowcaseEventListener, AdapterView.OnItemClickListener {

    private ProgressDialog pDialog;
    private ProgressDialog dialog;

    private int groupId;
    JSONParser jParser = new JSONParser();

    ArrayList<String> groupsList = new ArrayList<>();

    private static String url_all_groups = "http://radoslavbonev.net/get_all_groups.php";
    private static String url_request_membership = "http://radoslavbonev.net/request_membership.php";
    private static String url_requests = "http://radoslavbonev.net/get_requests_user.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "users";
    private static final String TAG_PID = "group_id";
    private static final String TAG_NAME = "group_name";
    private static final String TAG_USER_ID = "user_id";

    private ArrayList<Integer> ids = new ArrayList<>();

    JSONArray groups = null;

    ArrayAdapter<String> adapter;

    private ArrayList<String> forbiddenGroups;

    final String PREFS_NAME = "allGroupsTip";
    final String PREF_VERSION_CODE_KEY = "version_code";
    final int DOESNT_EXIST = -1;

    int currentVersionCode = 0;


    private String name;
    ShowcaseView sv;
    ListView listView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_groups);


        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setMessage(R.string.select_the_group);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog showTheDialog = dialogBuilder.create();
        showTheDialog.show();

        TextView messageView = (TextView)showTheDialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        HardcodedListAdapter guideAdapter = new HardcodedListAdapter(this);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(guideAdapter);
        listView.setOnItemClickListener(this);


        Button createGroup;
        createGroup = (Button) findViewById(R.id.createGroup);
        createGroup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), newGroup.class);
                startActivityForResult(intent, 666);
            }
        });


        new LoadAllGroups().execute();

        ListView lv = getListView();


        lv.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String groupToApplyName = ((TextView) view.findViewById(R.id.name)).getText()
                        .toString();

                groupId = ids.get(groupsList.indexOf(groupToApplyName));

                System.out.println("id to apply " + ids.get(groupsList.indexOf(groupToApplyName)));


                if(groupNotAppliedOrAlreadyJoined()) {
                    new requestMembership().execute();
                }
                else
                {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(allGroups.this);
                    dialogBuilder.setMessage(R.string.already_a_member);
                    dialogBuilder.setCancelable(false);
                    dialogBuilder.setPositiveButton(R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.dismiss();
                                }
                            });


                    AlertDialog showTheAlert = dialogBuilder.create();
                    showTheAlert.show();
                    Toast.makeText(allGroups.this, R.string.already_a_member, Toast.LENGTH_LONG).show();
                }

            }
        });
        EditText inputSearch = (EditText) findViewById(R.id.inputSearch);

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                try {
                    adapter.getFilter().filter(cs);
                }
                catch (Exception exception)
                {
                    System.out.println(exception);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });





        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        //I would be able to easily check whether it is a normal run, a new version is installed, or this is the first run
        //if I need
        if (savedVersionCode == DOESNT_EXIST) {
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);

            ViewTarget target = new ViewTarget(R.id.createGroup, this);
            sv = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(target)
                    .setContentTitle(getString(R.string.own_group))
                    .setContentText(getString(R.string.click_here_to_create_own_group))
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .setShowcaseEventListener(this)
                    .build();
            sv.setButtonPosition(lps);

        }
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();


        forbiddenGroups = new ArrayList<>(userDetails.groups);
        new LoadRequestedGroups().execute();

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_all_groups, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent intent;
        switch (item.getItemId()) {
            case R.id.new_group:
                intent = new Intent(this, newGroup.class);
                startActivityForResult(intent, 666);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean groupNotAppliedOrAlreadyJoined() {
        for(int i=0; i<forbiddenGroups.size(); i++) {
            if(Integer.valueOf(forbiddenGroups.get(i)) == groupId) {
                return false;
            }
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }

        if((requestCode == 666) && (resultCode == RESULT_OK))
        {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage(getString(R.string.following_group_created) + data.getStringExtra("group_name") + getString(R.string.enjoy_my_service));
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                        }
                    });


            AlertDialog showTheDialog = dialogBuilder.create();
            showTheDialog.show();

            TextView messageView = (TextView)showTheDialog.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);

        }

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        recreate();
    }


    class LoadAllGroups extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(allGroups.this);
            pDialog.setMessage(getString(R.string.loading_groups));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jParser.makeHttpRequest(url_all_groups, "GET", params);

            Log.d("All Users: ", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                System.out.println("success:   " + success);

                if (success == 1) {

                    groups = json.getJSONArray(TAG_PRODUCTS);

                    for (int i = 0; i < groups.length(); i++) {
                        JSONObject c = groups.getJSONObject(i);

                        int id = Integer.parseInt(c.getString(TAG_PID));
                        if(id != -1)
                        {
                            String name = c.getString(TAG_NAME);

                            ids.add(id);
                            System.out.println("id:   "  + id + "   name:   " + name);

                            groupsList.add(name);
                        }

                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    String[] groups = groupsList.toArray(new String[groupsList.size()]);
                    adapter = new ArrayAdapter<>(allGroups.this, R.layout.list_item_groups, R.id.name, groups);
                    setListAdapter(adapter);
                }
            });

        }

    }















    class requestMembership extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(allGroups.this);
            pDialog.setMessage(getString(R.string.sending));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {


            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_PID, String.valueOf(groupId)));
            params.add(new BasicNameValuePair(TAG_USER_ID, String.valueOf(userDetails.loggedUserID)));
            params.add(new BasicNameValuePair("user_name", String.valueOf(userDetails.loggedUserName)));



            JSONObject json = jParser.makeHttpRequest(url_request_membership,
                    "POST", params);
            try {
              int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    Intent i = new Intent();

                    setResult(100, i);


                    finish();

                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }



        protected void onPostExecute(String file_url) {

            pDialog.dismiss();
        }
    }





    class LoadRequestedGroups extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(allGroups.this);
            dialog.setMessage(getString(R.string.loading_requests));
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.show();

        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("user", String.valueOf(userDetails.loggedUserID)));


            JSONObject json = jParser.makeHttpRequest(url_requests, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);


                System.out.println("success:   " + success);

                if (success == 1) {

                    JSONArray products = json.getJSONArray("users");


                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);


                        String for_group = c.getString("for_group");
                        forbiddenGroups.add(for_group);



                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            dialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {

                    for (int i = 0; i<forbiddenGroups.size(); i++)
                    {
                        System.out.println("forbidden group: " + forbiddenGroups.get(i));
                    }

                }
            });

        }


    }














    @Override
    public void onClick(View view) {

        int viewId = view.getId();
        switch (viewId) {
            case R.id.btnLogIn:
                if (sv.isShown()) {
                    sv.setStyle(R.style.CustomShowcaseTheme);
                } else {
                    sv.show();
                }
                break;
        }
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            listView.setAlpha(1f);
        }
    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

    }


    private static class HardcodedListAdapter extends ArrayAdapter {


        public HardcodedListAdapter(Context context) {
            super(context, R.layout.activity_log_in);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_log_in, parent, false);
            }

            return convertView;
        }
    }

}