package com.radoslav.socialreminder;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class groupTasks extends AppCompatActivity implements navigationDrawerCallbacks {

    private Toolbar mToolbar;
    private navigationDrawerFragment mNavigationDrawerFragment;


    private scrimInsetsFrameLayout scrimInsetsFrameLayout;


    JSONParser jParser = new JSONParser();


    ArrayList<HashMap<String, String>> productsList;



    private JSONArray products = null;


    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_product_details = "http://radoslavbonev.net/get_all_tasks.php";
    performSync performSync;


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERS = "users";


    private ArrayList<String> taskTags = new ArrayList<>();
    private ArrayList<String> taskAssignedDays = new ArrayList<>();
    private ArrayList<String> taskDeadlines = new ArrayList<>();

    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_tasks_topdrawer);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mNavigationDrawerFragment = (navigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        mNavigationDrawerFragment.setTxtUserEmail(userDetails.loggedEmail);
        mNavigationDrawerFragment.setTxtUsername(userDetails.loggedUserName);
        mNavigationDrawerFragment.setProfilePic();

        scrimInsetsFrameLayout = (scrimInsetsFrameLayout) findViewById(R.id.scrimInsetsFrameLayout);

        scrimInsetsFrameLayout.bringToFront();

        new LoadAllTasks().execute();





    }


    @Override
    public void onNavigationDrawerItemSelected(int position) {


    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group_tasks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    class LoadAllTasks extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(groupTasks.this);
            pDialog.setMessage(getString(R.string.loading_tasks));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            System.out.println("group:   " + String.valueOf(userDetails.currentGroupChosenID));
            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));


            JSONObject json = jParser.makeHttpRequest(url_product_details, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);


                if (success == 1) {

                    products = json.getJSONArray(TAG_USERS);


                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        String tags = c.getString("task_tags");
                        taskTags.add(tags);

                        System.out.println("tags:   " + tags);

                        String assignedDay = c.getString("assigned_day");
                        taskAssignedDays.add(assignedDay);


                        String deadline = c.getString("deadline");
                        taskDeadlines.add(deadline);



                    }
                } else {

                    Intent i = new Intent(getApplicationContext(),
                            newGroup.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    lv = (ListView) findViewById(R.id.listView);
                    lv.setAdapter(new VersionAdapter(groupTasks.this));
                    lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

                }
            });

        }


    }



    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(groupTasks activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return taskTags.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item, null);
            }

            ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView tvTitle = (TextView) listItem.findViewById(R.id.title);
            TextView tvDesc = (TextView) listItem.findViewById(R.id.desc);
         //   TextView tvTags = (TextView) listItem.findViewById(R.id.tagsText);

            //  iv.setBackgroundResource(thumb[pos]);  // **************
            tvTitle.setText(taskTags.get(pos));
            tvDesc.setText(taskAssignedDays.get(pos));
         //   tvTags.setText(internalTaskTags.get(pos));

            return listItem;
        }

    }



}
