package com.radoslav.socialreminder;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.os.StrictMode;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class home extends AppCompatActivity implements navigationDrawerCallbacks {

    private Toolbar mToolbar;
    private navigationDrawerFragment mNavigationDrawerFragment;

    JSONParser jParser = new JSONParser();

    private Boolean toUpdate = false;

    private ArrayList<String> localTags = new ArrayList<>();
    private ArrayList<String> localAssignedDay = new ArrayList<>();
    private ArrayList<String> localDeadline = new ArrayList<>();


    private JSONArray products = null;


    private ProgressDialog pDialog;
    private ProgressDialog dialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_product_details = "http://radoslavbonev.net/get_all_tasks.php";
    private static String url_all_groups = "http://radoslavbonev.net/get_all_groups.php";

    performSync performSync;


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERS = "users";


    private ArrayList<String> taskNames = new ArrayList<>();
    private ArrayList<String> taskTags = new ArrayList<>();
    private ArrayList<String> taskAssignedDays = new ArrayList<>();
    private ArrayList<String> taskDeadlines = new ArrayList<>();

    private TextView userLoggedIn, groupChosen, missedTasks;

    private scrimInsetsFrameLayout scrimInsetsFrameLayout;
    private String[] groups;

    private ArrayList<String> groupNames = new ArrayList<>();
    private static final String TAG_PID = "group_id";
    private static final String TAG_NAME = "group_name";

    private ArrayList<String> groupsArray;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_topdrawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        mNavigationDrawerFragment = (navigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        mNavigationDrawerFragment.setTxtUserEmail("no");
        mNavigationDrawerFragment.setTxtUsername("NO");
        mNavigationDrawerFragment.setProfilePic();

        scrimInsetsFrameLayout = (scrimInsetsFrameLayout) findViewById(R.id.scrimInsetsFrameLayout);

        scrimInsetsFrameLayout.bringToFront();

        if(!isNetworkAvailable())
        {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage(R.string.no_network);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    });


            AlertDialog showTheAlert = dialogBuilder.create();
            showTheAlert.show();
        }
        else {



            if (!userDetails.userLoggedIn) {
                Intent intent = new Intent(this, logIn.class);
                startActivityForResult(intent, 1);
            }
        }

        userLoggedIn = (TextView) findViewById(R.id.userLoggedIn);
        groupChosen = (TextView) findViewById(R.id.chosenGroup);
        missedTasks = (TextView) findViewById(R.id.missedTasks);



        if(userDetails.loggedUserName.equals (""))
            {
                userLoggedIn.setText(getString(R.string.logged_in_as) + userDetails.loggedUserName);
            }




    }

    @Override
    protected void onRestart()
    {
        super.onRestart();

        userLoggedIn.setText(getString(R.string.logged_in_as) + userDetails.loggedUserName);

        if(!isNetworkAvailable())
        {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage(R.string.no_network);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                        }
                    });


            AlertDialog showTheDialog = dialogBuilder.create();
            showTheDialog.show();
        }
        else {

            if (!userDetails.userLoggedIn) {
                Intent intent = new Intent(this, logIn.class);
                startActivityForResult(intent, 1);
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.new_group:
                intent = new Intent(this, newGroup.class);
                startActivity(intent);
                return true;
            case R.id.list_groups:
                intent = new Intent(this, allGroups.class);
                startActivity(intent);
                return true;
            case R.id.editDetails:
                intent = new Intent(this, editUserDetails.class);
                startActivity(intent);
                return true;
            case R.id.managePeople:
                if(userDetails.manageGroupUsersPermission.get(userDetails.groupChosenOrder) == 1) {
                    intent = new Intent(this, allUsersPermissions.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), R.string.no_permission, Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.manageTags:
                if(userDetails.manageTagsPermission.get(userDetails.groupChosenOrder) == 1) {
                    intent = new Intent(this, addTags.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), R.string.no_permission, Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.requests:
                if(userDetails.manageGroupUsersPermission.get(userDetails.groupChosenOrder) == 1) {
                    intent = new Intent(this, membershipRequests.class);
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), R.string.no_permission, Toast.LENGTH_LONG).show();
                }
                return true;

            case R.id.settings:
                intent = new Intent(this, settings.class);
                startActivity(intent);
                return true;
            case R.id.logOut:
                userDetails.loggedUserID = 0;
                userDetails.currentGroupChosenID = 0;
                userDetails.userLoggedIn = false;
                userDetails.loggedUserName = "";
                userDetails.loggedEmail = "";
                userDetails.groupChosenOrder = 0;
                userDetails.groups.clear();
                userDetails.addTasksPermission.clear();
                userDetails.manageTagsPermission.clear();
                userDetails.manageGroupUsersPermission.clear();
                recreate();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        if(position == 1)
        {
                Intent intent = new Intent(this, myTasks.class);
                startActivity(intent);
        }
        else if(position == 2)
        {
            Intent intent = new Intent(this, groupTasks.class);
            startActivity(intent);
        }
        else if(position == 3)
        {
            Intent intent = new Intent(this, whatDoIMiss.class);
            startActivity(intent);
        }

    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if((requestCode == 1) && (resultCode == RESULT_OK))
        {
            mNavigationDrawerFragment.setTxtUserEmail(data.getStringExtra("email"));
            System.out.println("email to set " + data.getStringExtra("email"));
            mNavigationDrawerFragment.setTxtUsername(data.getStringExtra("name"));
            System.out.println("loading all tasks");
            new LoadAllProducts().execute();
            groupsArray = new ArrayList<>(userDetails.getGroups());

            new LoadAllGroups().execute();


        }
        else if ((requestCode == 1) && (resultCode == 720))
        {
            finish();
        }
        else
        {
            Toast.makeText(getApplicationContext(), R.string.not_logged_in, Toast.LENGTH_LONG).show();
        }

    }




    class LoadAllProducts extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(home.this);
            pDialog.setMessage(getString(R.string.loading_tasks));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            System.out.println("group:   " + String.valueOf(userDetails.currentGroupChosenID));
            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));



            JSONObject json = jParser.makeHttpRequest(url_product_details, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);


                if (success == 1) {

                    products = json.getJSONArray(TAG_USERS);


                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        String name = c.getString("task_name");
                        taskNames.add(name);

                        String tags = c.getString("task_tags");
                        taskTags.add(tags);

                        System.out.println("tags:   " + tags);

                        String assignedDay = c.getString("assigned_day");
                        taskAssignedDays.add(assignedDay);


                        String deadline = c.getString("deadline");
                        taskDeadlines.add(deadline);


                        System.out.println("DB says:   " + tags);



                    }



                    DBOperations dbOperations = new DBOperations(home.this);
                    Cursor cursor = dbOperations.getInformation(dbOperations);
                    try {
                        if (cursor != null) {


                            cursor.moveToFirst();
                            do {
                                localTags.add(cursor.getString(3));
                                localAssignedDay.add(cursor.getString(2));
                                localDeadline.add("14/07/1998");

                                System.out.println("My elements: " + cursor.getString(0) + "   " + cursor.getString(1) + "   " + cursor.getString(2) + "   " + cursor.getString(3));
                            }
                            while (cursor.moveToNext());


                            Looper.prepare();

                            performSync = new performSync(taskTags, taskAssignedDays, taskDeadlines, localTags, localAssignedDay, localDeadline, taskNames);

                            putInfo();
                            cursor.close();
                        }
                    }
                   catch (Exception e)
                    {
                        localTags.add("");
                        localAssignedDay.add("");
                        localDeadline.add("");

                        try {
                            Looper.prepare();
                            performSync = new performSync(taskTags, taskAssignedDays, taskDeadlines, localTags, localAssignedDay, localDeadline, taskNames);

                            putInfo();
                            cursor.close();
                        }
                        catch (Exception ex)
                        {
                            System.out.println("Exc:   " + ex);

                        }
                        System.out.println("Excc:   " + e);
                   }



                } else {

                    Intent i = new Intent(getApplicationContext(),
                            myTasks.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {


                }
            });

        }

    }

    public void putInfo()
    {
ArrayList<String> names = new ArrayList<>(performSync.getNames());
            ArrayList<String> assignedDay = new ArrayList<>(performSync.getAssignedDay());
        ArrayList<String>  tags = new ArrayList<>(performSync.getTags());
            DBOperations DB = new DBOperations(this);
        DBOperationsWhatDidImiss DBw = new DBOperationsWhatDidImiss(this);
        if(tags.size() > 0) {
            DBw.deleteEverything(DBw);
            for (int i = 0; i < tags.size(); i++) {
                DB.putInformation(DB, "Group: " + names.get(i), assignedDay.get(i), tags.get(i), String.valueOf(userDetails.currentGroupChosenID));
                DBw.putInformation(DBw, names.get(i), assignedDay.get(i), tags.get(i));
            }
            Toast.makeText(getBaseContext(), R.string.task_created, Toast.LENGTH_LONG).show();
        }

        final int NOTIFICATION_ID = 1;


if(tags.size() > 0) {
    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(home.this);


    Boolean notifications = settings.getBoolean("notifications_new_message", true);


    if (notifications) {
        Uri defaultSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        String sound = settings.getString("notifications_new_message_ringtone", "not chosen");

        Uri ring = Uri.parse(sound);

        NotificationCompat.Builder buildNotification = new NotificationCompat.Builder(this);
        buildNotification.setSmallIcon(R.drawable.ic_stat_notification);
        buildNotification.setContentTitle("You miss something!");
        String notificationText = getResources().getQuantityString(R.plurals.missed_tasks_notification, tags.size(), tags.size());
        buildNotification.setContentText(notificationText);
        System.out.println("ring:  "  + ring);
        if (!sound.equals("not chosen")) {
            buildNotification.setSound(ring);
        } else {
            buildNotification.setSound(defaultSound);
        }
        Boolean vibrate = settings.getBoolean("notifications_new_message", true);

        if (vibrate) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(600);
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(
                NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, buildNotification.build());
    }
}

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }




    class LoadAllGroups extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(home.this);
            dialog.setMessage(getString(R.string.loading_groups));
            dialog.setIndeterminate(false);
            dialog.setCancelable(false);
            dialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jParser.makeHttpRequest(url_all_groups, "GET", params);

            Log.d("All Users: ", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                System.out.println("success:   " + success);

                if (success == 1) {

                    JSONArray groups = json.getJSONArray(TAG_USERS);

                    for (int i = 0; i < groups.length(); i++) {
                        JSONObject c = groups.getJSONObject(i);

                        int id = Integer.parseInt(c.getString(TAG_PID));

                        for(int d = 0; d< groupsArray.size(); d++)
                        {
                            if(id == Integer.parseInt(groupsArray.get(d)))
                            {
                                String name = c.getString(TAG_NAME);
                                groupNames.add(name);
                                System.out.println("nnnaaammmmeee:   " + name);
                            }
                        }


                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            dialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    groups = new String[groupNames.size()];
                    groups = groupNames.toArray(groups);

                    final Spinner spinner = (Spinner) findViewById(R.id.groupsSpinner);

                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(home.this,   R.layout.spinner_item, groups);
                    spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(spinnerArrayAdapter);
                    spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            userDetails.currentGroupChosenID = Integer.parseInt(groupsArray.get(position));
                            userDetails.setGroupChosenOrder(position);

                            if (toUpdate) {
                                taskTags.clear();
                                taskAssignedDays.clear();
                                taskDeadlines.clear();
                                localTags.clear();
                                localAssignedDay.clear();
                                localDeadline.clear();
                                taskNames.clear();
                                new LoadAllProducts().execute();

                            }
                            toUpdate = true;

                            System.out.println("posss:   " + position);
                            System.out.println("group:   " + userDetails.currentGroupChosenID);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                }
            });

        }

    }

}



