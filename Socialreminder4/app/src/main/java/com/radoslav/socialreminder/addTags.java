package com.radoslav.socialreminder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class addTags extends AppCompatActivity implements View.OnClickListener,
        OnShowcaseEventListener, AdapterView.OnItemClickListener{

    JSONParser jParser = new JSONParser();


    private JSONArray products = null;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_create_tags = "http://radoslavbonev.net/create_tags.php";

    private static final String url_get_tags = "http://radoslavbonev.net/get_all_tags.php";

    private Toolbar mToolbar;


    private ArrayList<String> tagsID = new ArrayList<>();
    private ArrayList<String> tagsContent = new ArrayList<>();


    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCTS = "users";


    private TextView tvIDs, tvTags;

    private EditText tagsText;
    private Button createTags, reset;

    private ListView lv;
    private String currentID = "";

    private int numberOfTags = 0;

    final String PREFS_NAME = "addTagsTip";
    final String PREF_VERSION_CODE_KEY = "version_code";
    final int DOESNT_EXIST = -1;

    int currentVersionCode = 0;


    private String name;
    ShowcaseView sv;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_tags);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        HardcodedListAdapter adapter = new HardcodedListAdapter(this);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        tagsText = (EditText) findViewById(R.id.tagsText);

        reset = (Button) findViewById(R.id.reset);
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tagsText.setText("");
            }
        });

        createTags = (Button) findViewById(R.id.createTags);
        createTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tagsText.setError(null);

                if(notTooManyTags()) {
                    if (tagsValid()) {
                        if (tagsNotTooLong()) {
                            new createNewTags().execute();
                        } else {
                            tagsText.setError(getString(R.string.tags_too_many_chars));
                            Toast.makeText(getApplicationContext(), getString(R.string.tags_too_many_chars), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        tagsText.setError(getString(R.string.tags_not_valid));
                        Toast.makeText(getApplicationContext(), getString(R.string.tags_not_valid), Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    tagsText.setError(getString(R.string.tags_limit));
                    Toast.makeText(getApplicationContext(), getString(R.string.tags_limit), Toast.LENGTH_LONG).show();

                }
            }
        });


        reset = (Button) findViewById(R.id.reset);

        new LoadAllTags().execute();


        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        //I would be able to easily check whether it is a normal run, a new version is installed, or this is the first run
        //if I need
        if (savedVersionCode == DOESNT_EXIST) {
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);

            ViewTarget target = new ViewTarget(R.id.createTags, this);
            sv = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(target)
                    .setContentTitle(getString(R.string.tags_what_identifies_a_task))
                    .setContentText(getString(R.string.addTags_user_guide))
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .setShowcaseEventListener(this)
                    .build();
            sv.setButtonPosition(lps);

        }
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();

    }

    private boolean notTooManyTags()
    {
        return numberOfTags < 3;
    }

    private boolean tagsValid()
    {
        String tags = tagsText.getText().toString();
        String[] singleTags = tags.split(", ");
        return singleTags.length > 1;
    }
    private boolean tagsNotTooLong()
    {
        String tags = tagsText.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<100 && chars>4;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_add_tags, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }







    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(addTags activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return tagsID.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.tags_list_item, null);
            }

         //   ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
             tvIDs = (TextView) listItem.findViewById(R.id.tagsId);
             tvTags = (TextView) listItem.findViewById(R.id.tagsText);



            //  iv.setBackgroundResource(thumb[pos]);  // **************

            tvIDs.setText(tagsID.get(pos));
            tvTags.setText(tagsContent.get(pos));

            return listItem;
        }

    }









    class createNewTags extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(addTags.this);
            pDialog.setMessage(getString(R.string.creating_tags));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {


            String tags = tagsText.getText().toString();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("tags", tags));
            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));
            params.add(new BasicNameValuePair("user", String.valueOf(userDetails.loggedUserID)));



            JSONObject json = jsonParser.makeHttpRequest(url_create_tags,
                    "POST", params);


            Log.d("Create Response", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                System.out.println("success:   " + success);

                if (success == 1) {
                    pDialog.dismiss();

                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            recreate();
        }

    }



    class LoadAllTags extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(addTags.this);
            pDialog.setMessage(getString(R.string.loading_tags));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            try {
                pDialog.show();
            }
            catch (Exception e)
            {

            }
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));
            JSONObject json = jParser.makeHttpRequest(url_get_tags, "GET", params);

            Log.d("All Users: ", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                System.out.println("success:   " + success);

                if (success == 1) {

                    products = json.getJSONArray(TAG_PRODUCTS);

                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        String id = c.getString("tags_id");
                        tagsID.add(id);

                        String tags = c.getString("tags_group");
                        tagsContent.add(tags);

                        System.out.println("id:   " + id + "   tags:   " + tags);
                    }
                } else {


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
try {
    pDialog.dismiss();
}
catch (Exception e)
{

}

            runOnUiThread(new Runnable() {
                public void run() {

                    lv = (ListView) findViewById(R.id.listView);
                    lv.setAdapter(new VersionAdapter(addTags.this));
                    lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                                long arg3) {
                            // TODO Auto-generated method stub

                            int pos = arg2;

                            currentID = ((TextView) arg1.findViewById(R.id.tagsId)).getText()
                                    .toString();


                            Intent intent = new Intent(addTags.this, editTags.class);
                            intent.putExtra("id", currentID);
                            startActivityForResult(intent, 33);

                        }
                    });
                    numberOfTags = tagsID.size();

                }
            });

        }


    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if((requestCode == 33) && (resultCode == 103))
        {
            String oldTag = data.getStringExtra("oldTag");
            String newTag = data.getStringExtra("newTag");

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage(getString(R.string.successfully_changed) + " " + oldTag  + "\n" + getString(R.string.to) + "\n" + newTag);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            recreate();

                        }
                    });


            AlertDialog showTheDialog = dialogBuilder.create();
            showTheDialog.show();

            TextView messageView = (TextView)showTheDialog.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);

        }
        else if((requestCode == 33) && (resultCode == 106))
        {
            String oldTag = data.getStringExtra("oldTag");

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setMessage(getString(R.string.successfully_deleted) + "\n" + oldTag);
            dialogBuilder.setCancelable(false);
            dialogBuilder.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            recreate();

                        }
                    });


            AlertDialog showTheDialog = dialogBuilder.create();
            showTheDialog.show();

            TextView messageView = (TextView)showTheDialog.findViewById(android.R.id.message);
            messageView.setGravity(Gravity.CENTER);
        }


    }






    @Override
    public void onClick(View view) {

        int viewId = view.getId();
        switch (viewId) {
            case R.id.createTags:
                if (sv.isShown()) {
                    sv.setStyle(R.style.CustomShowcaseTheme);
                } else {
                    sv.show();
                }
                break;
        }
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            listView.setAlpha(1f);
        }
    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

    }


    private static class HardcodedListAdapter extends ArrayAdapter {


        public HardcodedListAdapter(Context context) {
            super(context, R.layout.activity_add_tags);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_add_tags, parent, false);
            }

            return convertView;
        }
    }

}
