package com.radoslav.socialreminder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class newGroup extends Activity {

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();
    EditText inputName;
    EditText inputDesc;

    JSONArray groups = null;

    ArrayList<String> groupsNames = new ArrayList<>();


    private static String url_create_product = "http://radoslavbonev.net/create_group.php";
    private static String url_all_groups = "http://radoslavbonev.net/get_all_groups.php";

    private static final String TAG_SUCCESS = "success";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_group);



        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        inputName = (EditText) findViewById(R.id.groupName);
        inputDesc = (EditText) findViewById(R.id.groupDescription);

        Button btnCreate = (Button) findViewById(R.id.create);


        btnCreate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                inputName.setError(null);
                inputDesc.setError(null);

                    if (nameNotTooLong()) {

                        if (descriptionNotTooLong()) {
                            new LoadAllGroups().execute();

                        } else {
                            inputDesc.setError(getString(R.string.long_group_description));
                        }
                    } else {
                        inputName.setError(getString(R.string.long_group_name));
                    }

            }

        });

        Button btnReset = (Button) findViewById(R.id.reset);

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inputName.setText("");
                inputDesc.setText("");
            }
        });
    }

    private boolean nameAvailable()
    {
        return groupsNames.indexOf(inputName.getText().toString()) == -1;
    }
    private boolean nameNotTooLong()
    {
        String tags = inputName.getText().toString();
        int chars = tags.codePointCount(0, tags.length()); // returns 5

        return chars<24 && chars>3;
    }


    private boolean descriptionNotTooLong()
    {
        String tags = inputDesc.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<100 && chars > 10;
    }

    class CreateNewGroup extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(newGroup.this);
            pDialog.setMessage(getString(R.string.creating_group));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            userDetails userDetails = new userDetails();

            String name = inputName.getText().toString();
            String description = inputDesc.getText().toString();
            String creator = String.valueOf(userDetails.getLoggedUserID());

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("description", description));
            params.add(new BasicNameValuePair("creator", creator));


            JSONObject json = jsonParser.makeHttpRequest(url_create_product,
                    "POST", params);

            Log.d("Create Response", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    Intent intent = new Intent();
                    intent.putExtra("group_name", name);
                    setResult(RESULT_OK, intent);

                    finish();
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }

    class LoadAllGroups extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(newGroup.this);
            pDialog.setMessage(getString(R.string.loading_groups));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
            groupsNames.clear();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            JSONObject json = jsonParser.makeHttpRequest(url_all_groups, "GET", params);

            Log.d("All Users: ", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                System.out.println("success:   " + success);

                if (success == 1) {

                    groups = json.getJSONArray("users");

                    for (int i = 0; i < groups.length(); i++) {
                        JSONObject c = groups.getJSONObject(i);


                            String name = c.getString("group_name");
                            System.out.println("name:   " + name);
                            groupsNames.add(name);

                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            if(nameAvailable())
            {
                new CreateNewGroup().execute();
            }
            else
            {
                inputName.setError(getString(R.string.group_exists));
            }
        }

    }
}
