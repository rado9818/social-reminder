package com.radoslav.socialreminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Rado on 17/07/2015.
 **/
public class DBOperationsWhatDidImiss extends SQLiteOpenHelper {

    public static final int DBVersion = 1;
    public String query = "CREATE TABLE " + tableDataWhatDidImiss.TableInfo.TABLE_NAME +"(" + tableDataWhatDidImiss.TableInfo.TASK_ID + " INTEGER PRIMARY KEY, " + tableDataWhatDidImiss.TableInfo.TASK_CONTENT + " TEXT, " + tableDataWhatDidImiss.TableInfo.TASK_DATE + " TEXT, " + tableDataWhatDidImiss.TableInfo.TASK_TAGS + " TEXT);";


    public DBOperationsWhatDidImiss(Context context) {
        super(context, tableDataWhatDidImiss.TableInfo.DATABASE_NAME, null, DBVersion);
        Log.d("DBOperations", "DB was created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);
        Log.d("DBOperations", "Table was created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void putInformation(DBOperationsWhatDidImiss dbOperations, String content, String date, String tags)
    {
        SQLiteDatabase SQ = dbOperations.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(tableDataWhatDidImiss.TableInfo.TASK_CONTENT, content);
        contentValues.put(tableDataWhatDidImiss.TableInfo.TASK_DATE, date);
        contentValues.put(tableDataWhatDidImiss.TableInfo.TASK_TAGS, tags);
        SQ.insert(tableDataWhatDidImiss.TableInfo.TABLE_NAME, null, contentValues);
        Log.d("DBOperations", "Inserted a row");

    }

    public void deleteInformation(DBOperationsWhatDidImiss dbOperations, int currentID)
    {
        String selection = tableDataWhatDidImiss.TableInfo.TASK_ID + " LIKE ?";
        String[] args = {String.valueOf(currentID)};
        SQLiteDatabase SQ = dbOperations.getWritableDatabase();
        SQ.delete(tableDataWhatDidImiss.TableInfo.TABLE_NAME, selection, args);
        Log.d("DBOperations", "Deleted");

    }

    public void deleteEverything(DBOperationsWhatDidImiss dbOperations)
    {
        SQLiteDatabase db = dbOperations.getWritableDatabase();
        String query = "DELETE FROM " + tableDataWhatDidImiss.TableInfo.TABLE_NAME;
        db.execSQL(query); //delete all rows in a table
        db.close();

    }

    public void updateInfo(DBOperationsWhatDidImiss dbOperations, int currentID, String newMyContent, String newMyDate)
    {
        SQLiteDatabase SQ = dbOperations.getWritableDatabase();
        String selection = tableDataWhatDidImiss.TableInfo.TASK_ID + " LIKE ?";
        String args[] = {String.valueOf(currentID)};
        ContentValues values = new ContentValues();
        values.put(tableDataWhatDidImiss.TableInfo.TASK_CONTENT, newMyContent);
        values.put(tableDataWhatDidImiss.TableInfo.TASK_DATE, newMyDate);
        SQ.update(tableDataWhatDidImiss.TableInfo.TABLE_NAME, values, selection, args);
    }

    public Cursor getInformation(DBOperationsWhatDidImiss dbOperations)
    {
        SQLiteDatabase SQ = dbOperations.getReadableDatabase();
        String[] columns = {tableDataWhatDidImiss.TableInfo.TASK_ID, tableDataWhatDidImiss.TableInfo.TASK_CONTENT, tableDataWhatDidImiss.TableInfo.TASK_DATE, tableDataWhatDidImiss.TableInfo.TASK_TAGS};
        Cursor cursor = SQ.query(tableDataWhatDidImiss.TableInfo.TABLE_NAME, columns, null, null, null, null, null);
        return cursor;
    }

}
