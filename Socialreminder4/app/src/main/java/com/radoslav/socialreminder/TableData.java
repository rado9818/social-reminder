package com.radoslav.socialreminder;

import android.provider.BaseColumns;

/**
 * Created by Rado on 17/07/2015.
 */
public class tableData {
    public tableData()
    {

    }

    public static abstract class TableInfo implements BaseColumns
    {
        public static final String TASK_CONTENT = "task_content";
        public static final String TASK_TAGS = "task_tags";
        public static final String TASK_GROUP = "task_group";
        public static final String TASK_DATE = "task_date";
        public static final String TASK_ID = "task_id";
        public static final String DATABASE_NAME = "myTasks";
        public static final String TABLE_NAME = "tasks";
    }
}
