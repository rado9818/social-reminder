package com.radoslav.socialreminder;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class whatDoIMiss extends AppCompatActivity implements navigationDrawerCallbacks {

    private Toolbar mToolbar;
    private navigationDrawerFragment mNavigationDrawerFragment;

    ArrayList<String> taskID = new ArrayList<>();
    ArrayList<String> taskContent = new ArrayList<>();
    ArrayList<String> taskDate = new ArrayList<>();
    ArrayList<String> internalTaskTags = new ArrayList<>();


    private ListView lv;
    private scrimInsetsFrameLayout scrimInsetsFrameLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_what_did__i_miss_topdrawer);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        mNavigationDrawerFragment = (navigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        mNavigationDrawerFragment.setTxtUserEmail(userDetails.loggedEmail);
        mNavigationDrawerFragment.setTxtUsername(userDetails.loggedUserName);
        mNavigationDrawerFragment.setProfilePic();

        scrimInsetsFrameLayout = (scrimInsetsFrameLayout) findViewById(R.id.scrimInsetsFrameLayout);

        scrimInsetsFrameLayout.bringToFront();
        try {
            showInfo();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), R.string.no_entries,
                    Toast.LENGTH_LONG).show();
        }

        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(new VersionAdapter(this));
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub

                int pos = arg2;

                LayoutInflater layoutInflator = getLayoutInflater();

                View layout = layoutInflator.inflate(R.layout.custom_toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));


                // ImageView iv = (ImageView) layout.findViewById(R.id.toast_iv);
                TextView tv = (TextView) layout.findViewById(R.id.toast_tv);

                // iv.setBackgroundResource(thumb[pos]); //****************************
                tv.setText(taskContent.get(pos));

            }
        });

    }


    private void showInfo() {
        DBOperationsWhatDidImiss dbOperations = new DBOperationsWhatDidImiss(this);
        Cursor cursor = dbOperations.getInformation(dbOperations);
        cursor.moveToFirst();
        do {
            taskID.add(cursor.getString(0));
            taskContent.add(cursor.getString(1));
            taskDate.add(cursor.getString(2));
            internalTaskTags.add(cursor.getString(3));


            System.out.println("My elements: " + cursor.getString(0) + "   " + cursor.getString(1) + "   " + cursor.getString(2) + "   " + cursor.getString(3));
        }
        while (cursor.moveToNext());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_what_did__i_miss, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {


    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }


    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(whatDoIMiss activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return taskContent.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item, null);
            }

            ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView tvTitle = (TextView) listItem.findViewById(R.id.title);
            TextView tvDesc = (TextView) listItem.findViewById(R.id.desc);
            TextView tvTags = (TextView) listItem.findViewById(R.id.tagsText);

            //  iv.setBackgroundResourcge(thumb[pos]);  // **************
            tvTitle.setText(taskContent.get(pos));
            tvDesc.setText(taskDate.get(pos));
            tvTags.setText(internalTaskTags.get(pos));

            return listItem;
        }
    }
}
