package com.radoslav.socialreminder;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;
import com.nispok.snackbar.listeners.ActionClickListener;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class myTasks extends AppCompatActivity implements navigationDrawerCallbacks, View.OnClickListener,
        OnShowcaseEventListener, AdapterView.OnItemClickListener {

    private Toolbar mToolbar;
    private navigationDrawerFragment mNavigationDrawerFragment;
    private scrimInsetsFrameLayout scrimInsetsFrameLayout;

    private Spinner spinner1, spinner2, spinner3;

    private String lastDeletedContent, lastDeletedAssignedDay, lastDeletedTags;


    JSONParser jParser = new JSONParser();


    private JSONArray products = null;


    EditText content;
    String myContent, myDate;
    Button add, delete, update;
    Context context = this;
    String newMyContent, newMyDate;

    private int currentID = -1;

    ArrayList<String> taskID = new ArrayList<>();
    ArrayList<String> taskContent = new ArrayList<>();
    ArrayList<String> taskDate = new ArrayList<>();
    ArrayList<String> internalTaskTags = new ArrayList<>();


    private ArrayList<String> taskTags = new ArrayList<>();
    private ArrayList<String> taskAssignedDays = new ArrayList<>();
    private ArrayList<String> taskDeadlines = new ArrayList<>();


    private ListView lv;

    private View lastView;

int actionPerformed = 0;
   /* private int thumb[] = { R.drawable.cupcake, R.drawable.donut,
            R.drawable.eclair, R.drawable.froyo, R.drawable.gingerbread,
            R.drawable.honeycomb, R.drawable.icecreamsandwich,
            R.drawable.jellybean, };*/

    private checkFromMyTasks checkFromMyTasks;


    private String tagsToBeSent;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_product_details = "http://radoslavbonev.net/get_all_tasks.php";
    private static final String url_create_task = "http://radoslavbonev.net/create_task.php";
    private static final String url_load_tags = "http://radoslavbonev.net/get_all_tags.php";




    private static final String TAG_SUCCESS = "success";
    private static final String TAG_USERS = "users";



    private ArrayList<String> tagsForSpinners = new ArrayList<>();

    private TextView output;
    private Button changeDate;

    private int year;
    private int month;
    private int day;

    static final int datePickerID = 6;
    private String assigned;

    final String PREFS_NAME = "myTasksTip";
    final String PREF_VERSION_CODE_KEY = "version_code";
    final int DOESNT_EXIST = -1;

    private String name;
    ShowcaseView sv;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_tasks_topdrawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);


        HardcodedListAdapter adapter = new HardcodedListAdapter(this);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);



        scrimInsetsFrameLayout = (scrimInsetsFrameLayout) findViewById(R.id.scrimInsetsFrameLayout);

        scrimInsetsFrameLayout.bringToFront();

        mNavigationDrawerFragment = (navigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        mNavigationDrawerFragment.setTxtUserEmail(userDetails.loggedEmail);
        mNavigationDrawerFragment.setTxtUsername(userDetails.loggedUserName);
        mNavigationDrawerFragment.setProfilePic();

        new LoadAllTasks().execute();


        DBOperations DB = new DBOperations(context);


        content = (EditText) findViewById(R.id.taskContent);

        output = (TextView) findViewById(R.id.output);
        changeDate = (Button) findViewById(R.id.changeDate);

        add = (Button) findViewById(R.id.submitButton);
        delete = (Button) findViewById(R.id.deleteButton);
        update = (Button) findViewById(R.id.updatButton);

        try{
            showInfo();
        }
        catch (Exception e)
        {
            Toast.makeText(getApplicationContext(), R.string.no_entries,
                    Toast.LENGTH_LONG).show();        }


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(contentNotTooLong()) {
                myContent = content.getText().toString();
                myDate = assigned;


                    if (tagsForSpinners.size() == 1) {
                        tagsToBeSent = spinner1.getSelectedItem().toString();

                    }

                    if (tagsForSpinners.size() == 2) {


                        tagsToBeSent = spinner1.getSelectedItem().toString() + ", " + spinner2.getSelectedItem().toString();
                    }

                    if (tagsForSpinners.size() == 3) {

                        tagsToBeSent = spinner1.getSelectedItem().toString() + ", " + spinner2.getSelectedItem().toString() + ", " + spinner3.getSelectedItem().toString();


                    }

                DBOperations DB = new DBOperations(context);
                DB.putInformation(DB, myContent, myDate, tagsToBeSent, String.valueOf(userDetails.currentGroupChosenID));
                Toast.makeText(getBaseContext(), R.string.task_created, Toast.LENGTH_LONG).show();
                    if(userDetails.addTasksPermission.get(userDetails.groupChosenOrder) == 1) {

                        Boolean toAdd = true;

                        for (int i = 0; i < taskTags.size(); i++) {
                            checkFromMyTasks = new checkFromMyTasks(1, tagsToBeSent, assigned, "14/07/1998",
                                    taskTags.get(i), taskAssignedDays.get(i), taskDeadlines.get(i));
                            double chance = checkFromMyTasks.getChanceOfMatching();
                            if (chance >= 80) {
                                toAdd = false;
                            }

                            System.out.println("chance in myTasks333:   " + checkFromMyTasks.getChanceOfMatching());


                        }

                        System.out.println("do add?????:   " + toAdd);

                        if (toAdd) {
                            new createNewTask().execute();
                        }

                    }

                showInfo();
                recreate();
            }
                else{
                    if(!contentNotTooLong())
                    {
                        content.setError(getString(R.string.task_name_too_long));
                    }

                }
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentID != -1) {


                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle(getString(R.string.confirmation));
                    alert.setMessage(getString(R.string.delete_confirmation));
                    alert.setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            myContent = content.getText().toString();
                            myDate = assigned;
                            DBOperations DB = new DBOperations(context);
                            if (currentID != -1) {
                                actionPerformed = 3;
                                DB.deleteInformation(DB, Integer.parseInt(taskID.get(currentID)));

                            }
                            currentID = -1;
                            try {
                                showInfo();
                            } catch (Exception exception) {
                                Toast.makeText(getApplicationContext(), R.string.deleted_last_task, Toast.LENGTH_LONG).show();

                            }

                            recreate();
                            

                        }

                    });



                    alert.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });

                    AlertDialog dialog = alert.show();

                }
                else
                {
                    Toast.makeText(getApplicationContext(), R.string.delete_warning, Toast.LENGTH_LONG).show();
                }
            }
        });


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(currentID != -1) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    LinearLayout layout = new LinearLayout(context);
                    layout.setOrientation(LinearLayout.VERTICAL);


                    alert.setTitle(getString(R.string.update));
                    alert.setMessage(getString(R.string.enterNewValues));

                    final EditText input = new EditText(context);
                    input.setHint(getString(R.string.newTaskContentHint));
                    layout.addView(input);

                    final EditText input2 = new EditText(context);
                    input2.setHint(getString(R.string.newTaskDateHint));
                    layout.addView(input2);

    input.setText(taskContent.get(currentID));
    input2.setText(taskDate.get(currentID));


                    alert.setView(layout);


                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            newMyContent = input.getText().toString();
                            newMyDate = input2.getText().toString();
                            actualUpdate();
                            currentID = -1;
                        }
                    });

                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                        }
                    });

                    alert.show();

                }
                else
                {
                    Toast.makeText(context, getString(R.string.update_warning), Toast.LENGTH_LONG).show();
                }
            }
        });



        lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(new VersionAdapter(this));
        lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                if (lastView != null) {
                    lastView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                }


                LayoutInflater layoutInflator = getLayoutInflater();

                View layout = layoutInflator.inflate(R.layout.custom_toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));


                // ImageView iv = (ImageView) layout.findViewById(R.id.toast_iv);
                TextView tv = (TextView) layout.findViewById(R.id.toast_tv);

                // iv.setBackgroundResource(thumb[pos]); //****************************
                tv.setText(taskContent.get(arg2));

                currentID = arg2;

                arg1.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));

                lastView = arg1;
                lastDeletedContent = ((TextView) lastView.findViewById(R.id.title)).getText()
                        .toString();

                lastDeletedTags = ((TextView) lastView.findViewById(R.id.tagsText)).getText()
                        .toString();

                lastDeletedAssignedDay  = ((TextView) lastView.findViewById(R.id.desc)).getText()
                        .toString();
                System.out.println("currentID:   " + currentID);

            }
        });


        final Calendar calendar = Calendar.getInstance();
        year  = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day   = calendar.get(Calendar.DAY_OF_MONTH);


        output.setText(new StringBuilder()
                .append(year).append("-").
                        append(month + 1).append("-").
                        append(day));


        assigned = output.getText().toString();

        System.out.println(assigned);

        changeDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(datePickerID);

            }

        });



        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        //I would be able to easily check whether it is a normal run, a new version is installed, or this is the first run
        //if I need
        if (savedVersionCode == DOESNT_EXIST) {
            RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
            lps.setMargins(margin, margin, margin, margin);



            ViewTarget target = new ViewTarget(R.id.submitButton, this);
            sv = new ShowcaseView.Builder(this)
                    .withMaterialShowcase()
                    .setTarget(target)
                    .setContentTitle(getString(R.string.enrich_your_group))
                    .setContentText(getString(R.string.add_tasks_instructions))
                    .setStyle(R.style.CustomShowcaseTheme2)
                    .setShowcaseEventListener(this)
                    .build();
            sv.setButtonPosition(lps);

        }
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(actionPerformed == 3) {
            SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            prefs.edit().putInt("actionPerformed", 3).apply();
            prefs.edit().putString("lastDeletedContent", lastDeletedContent).apply();
            prefs.edit().putString("lastDeletedTags", lastDeletedTags).apply();
            prefs.edit().putString("lastDeletedAssignedDay", lastDeletedAssignedDay).apply();
            actionPerformed  = 0;
        }



    }

    private void deleteSnackBar()
{
    SnackbarManager.show(
            Snackbar.with(getApplicationContext())
                    .text(getString(R.string.task_has_been_deleted))
                    .actionLabel(getString(R.string.undo))
                    .actionColor(Color.rgb(255, 100, 0))
                    .swipeToDismiss(true)// action button label
                    .actionListener(new ActionClickListener() {
                        @Override
                        public void onActionClicked(Snackbar snackbar) {
                            DBOperations DB = new DBOperations(context);
                            if (!lastDeletedContent.equals("") && !lastDeletedAssignedDay.equals("") && !lastDeletedTags.equals("")) {
                                DB.putInformation(DB, lastDeletedContent, lastDeletedAssignedDay, lastDeletedTags, String.valueOf(userDetails.currentGroupChosenID));
                                SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                                prefs.edit().putInt("actionPerformed", 0).apply();
                                snackbar.dismiss();
                                recreate();
                            } else {
                                Toast.makeText(getApplicationContext(), "Error while undoing the deletion", Toast.LENGTH_LONG).show();
                            }
                        }
                    }) // action button's ActionClickListener
            , this); // activity where it is displayed
}

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case datePickerID:

                return new DatePickerDialog(this, pickerListener, year, month,day);
        }
        return null;
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("on startttt");
        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int action = prefs.getInt("actionPerformed", 0);
        System.out.println(action);
        if(action == 3)
        {
            deleteSnackBar();
            lastDeletedContent = prefs.getString("lastDeletedContent", "");
            lastDeletedTags = prefs.getString("lastDeletedTags", "");
            lastDeletedAssignedDay = prefs.getString("lastDeletedAssignedDay", "");

            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("actionPerformed", 0);
            editor.apply();



        }
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            year  = selectedYear;
            month = selectedMonth;
            day   = selectedDay;

            output.setText(new StringBuilder()
                    .append(year).append("-").
                            append(month + 1).append("-").
                            append(day));


            assigned = output.getText().toString();

            System.out.println(assigned);
        }
    };

    private boolean contentNotTooLong()
    {
        String tags = content.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<100 && chars>3;
    }



    private void showInfo()
    {
        DBOperations dbOperations = new DBOperations(this);
        Cursor cursor = dbOperations.getInformation(dbOperations);
        cursor.moveToFirst();
        do{
            taskID.add(cursor.getString(0));
            taskContent.add(cursor.getString(1));
            taskDate.add(cursor.getString(2));
            internalTaskTags.add(cursor.getString(3));


            System.out.println("My elements: " + cursor.getString(0) + "   " + cursor.getString(1) + "   " + cursor.getString(2)+ "   " + cursor.getString(3));
        }
        while(cursor.moveToNext());
    }

    private void actualUpdate()
    {

        DBOperations DB = new DBOperations(context);
        DB.updateInfo(DB, Integer.parseInt(taskID.get(currentID)), newMyContent, newMyDate);
        showInfo();
        recreate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }



   @Override
    public void onNavigationDrawerItemSelected(int position) {


    }

    @Override
    public void onBackPressed() {
        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if((requestCode == 6))
        {
            if(resultCode != RESULT_OK)
            {
                Intent intent = new Intent(getApplicationContext(), addTags.class);
                startActivityForResult(intent, 6);
            }
            else
            {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                recreate();
            }

        }

    }

    private void createSpinners(int num)
    {

        if(num==0)
        {
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(myTasks.this);
            builder1.setMessage(R.string.no_tags_add_if_permission);
            builder1.setCancelable(false);
            builder1.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            Intent intent = new Intent(getApplicationContext(), addTags.class);
                            startActivityForResult(intent, 6);
                        }
                    });


            android.app.AlertDialog alert11 = builder1.create();
            alert11.show();

        }

        if (num >= 1) {
            String tags[] = parseTags(tagsForSpinners.get(0));

            spinner1 = (Spinner) findViewById(R.id.spinner1);

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   R.layout.spinner_item, tags);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinner1.setAdapter(spinnerArrayAdapter);
            spinner1.setVisibility(View.VISIBLE);
        }

        if (num >= 2) {
            String tags[] = parseTags(tagsForSpinners.get(1));

            spinner2 = (Spinner) findViewById(R.id.spinner2);

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   R.layout.spinner_item, tags);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinner2.setAdapter(spinnerArrayAdapter);
            spinner2.setVisibility(View.VISIBLE);

        }

        if (num >= 3) {
            String tags[] = parseTags(tagsForSpinners.get(2));

            spinner3 = (Spinner) findViewById(R.id.spinner3);

            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   R.layout.spinner_item, tags);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinner3.setAdapter(spinnerArrayAdapter);
            spinner3.setVisibility(View.VISIBLE);

        }
    }

    private String[] parseTags(String tags)
    {
        String[] forTheSpinner = tags.split(", ");
        return forTheSpinner;
    }


    class VersionAdapter extends BaseAdapter {

        private LayoutInflater layoutInflater;

        public VersionAdapter(myTasks activity) {
            // TODO Auto-generated constructor stub
            layoutInflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return taskContent.size();
        }

        @Override
        public Object getItem(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub

            View listItem = convertView;
            int pos = position;
            if (listItem == null) {
                listItem = layoutInflater.inflate(R.layout.list_item, null);
            }

            // Initialize the views in the layout
            ImageView iv = (ImageView) listItem.findViewById(R.id.thumb);
            TextView tvIDs = (TextView) listItem.findViewById(R.id.id);
            TextView tvTitle = (TextView) listItem.findViewById(R.id.title);
            TextView tvDesc = (TextView) listItem.findViewById(R.id.desc);
            TextView tvTags = (TextView) listItem.findViewById(R.id.tagsText);
String letter = "A";
if(taskContent.get(pos).substring(0, 6).equals("Group:"))
            {
                letter = String.valueOf(taskContent.get(pos).charAt(7)).toUpperCase();
            }
            else
{
    letter = String.valueOf(taskContent.get(pos).charAt(0)).toUpperCase();

}
            ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
// generate random color
            int color = generator.getRandomColor();
            TextDrawable drawable = TextDrawable.builder()
                    .buildRoundRect(letter, color, 20);



          //  iv.setBackgroundResource(thumb[pos]);  // **************
            tvIDs.setText(taskID.get(pos));
            tvTitle.setText(taskContent.get(pos));
            tvDesc.setText(taskDate.get(pos));
            tvTags.setText(internalTaskTags.get(pos));
            iv.setImageDrawable(drawable);

            return listItem;
        }

    }







    class LoadAllTasks extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(myTasks.this);
            pDialog.setMessage(getString(R.string.loading_tasks_group));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));
            JSONObject json = jParser.makeHttpRequest(url_product_details, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);


                if (success == 1) {

                    products = json.getJSONArray(TAG_USERS);


                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        String tags = c.getString("task_tags");
                        taskTags.add(tags);


                        String assignedDay = c.getString("assigned_day");
                        taskAssignedDays.add(assignedDay);


                        String deadline = c.getString("deadline");
                        taskDeadlines.add(deadline);


                        System.out.println("DB says:   " + tags);



                    }





                } else {
                    Intent i = new Intent(getApplicationContext(),
                            myTasks.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    new LoadAllTags().execute();

                }
            });

        }

    }








    class createNewTask extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(myTasks.this);
            pDialog.setMessage(getString(R.string.creating_task));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {


            String name = content.getText().toString();
            String tags = tagsToBeSent;
            String deadline = "14/07/1998";

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("user", String.valueOf(userDetails.loggedUserID)));
            params.add(new BasicNameValuePair("name", name));
            params.add(new BasicNameValuePair("tags", tags));
            params.add(new BasicNameValuePair("assigned", assigned));
            params.add(new BasicNameValuePair("deadline", deadline));
            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));



            JSONObject json = jsonParser.makeHttpRequest(url_create_task,
                    "POST", params);


            Log.d("Create Response", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                System.out.println("success:   " + success);

                if (success == 1) {

                } else {
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }

    }










    class LoadAllTags extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(myTasks.this);
            pDialog.setMessage( getString(R.string.loading_tags));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            params.add(new BasicNameValuePair("group", String.valueOf(userDetails.currentGroupChosenID)));
            JSONObject json = jParser.makeHttpRequest(url_load_tags, "GET", params);



            try {
                int success = json.getInt(TAG_SUCCESS);


                if (success == 1) {

                    products = json.getJSONArray(TAG_USERS);


                    for (int i = 0; i < products.length(); i++) {
                        JSONObject c = products.getJSONObject(i);

                        String tags = c.getString("tags_group");
                        tagsForSpinners.add(tags);


                    }




                } else if (products.length()==0){

                    Intent i = new Intent(getApplicationContext(),
                            addTags.class);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {

                    createSpinners(tagsForSpinners.size());
                }
            });

        }

    }









    @Override
    public void onClick(View view) {

        int viewId = view.getId();
        switch (viewId) {
            case R.id.btnLogIn:
                if (sv.isShown()) {
                    sv.setStyle(R.style.CustomShowcaseTheme);
                } else {
                    sv.show();
                }
                break;
        }
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            listView.setAlpha(1f);
        }
    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

    }


    private static class HardcodedListAdapter extends ArrayAdapter {


        public HardcodedListAdapter(Context context) {
            super(context, R.layout.activity_my_tasks_topdrawer);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_my_tasks_topdrawer, parent, false);
            }

            return convertView;
        }
    }




}