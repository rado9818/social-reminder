package com.radoslav.socialreminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.github.amlcurran.showcaseview.OnShowcaseEventListener;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;


public class logIn extends AppCompatActivity implements View.OnClickListener,
        OnShowcaseEventListener, AdapterView.OnItemClickListener {

    EditText textEmail;
    EditText textPassword;
    Button btnLogIn;
    Button btnReset;
    Button btnSignUp;

    private Boolean toClose = false;

    private Boolean toLogIn = false;
    private int clicks = 0;

    private JSONArray groups = null;

    private ProgressDialog pDialog, dialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_product_details = "http://radoslavbonev.net/get_user_pass.php";
    private static final String url_load_groups = "http://radoslavbonev.net/get_user_groups_permission.php";
    private static String url_create_product = "http://radoslavbonev.net/create_user.php";

    private Toolbar mToolbar;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCT = "user";
    private static final String TAG_ID = "id";
    private static final String TAG_NAME = "name";
    private static final String TAG_PRICE = "pass";
    private static final String TAG_SALT = "salt";

    private ArrayList<String> userGroups = new ArrayList<>();
    private ArrayList<Integer> addTasksPermission = new ArrayList<>();
    private ArrayList<Integer> manageTagsPermission = new ArrayList<>();
    private ArrayList<Integer> manageGroupUsersPermission = new ArrayList<>();
    private userDetails userDetails = new userDetails();

    final String PREFS_NAME = "logInTip";
    final String PREF_VERSION_CODE_KEY = "version_code";
    final int DOESNT_EXIST = -1;

    int currentVersionCode = 0;


    private String name;
    ShowcaseView sv;
    ListView listView;




    CallbackManager callbackManager;
    Button share,details;
    ShareDialog shareDialog;
    LoginButton login;
    ProfilePictureView profile;
    Dialog details_dialog;
    TextView details_txt;

    private String facebookEmail = "";
    private String facebookName = "";

    private Boolean accountExist = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_log_in);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        btnLogIn = (Button) findViewById(R.id.btnLogIn);

        HardcodedListAdapter adapter = new HardcodedListAdapter(this);
        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textEmail.setError(null);
                textPassword.setError(null);

                if (!textEmail.getText().toString().equals("") && !textPassword.getText().toString().equals("")) {

                    if (emailNotTooLong()) {


                        if (passwordNotTooLong()) {

                            new GetProductDetails().execute();
                        } else {
                            textPassword.setError(getString(R.string.password_too_long));
                        }
                    } else {
                        textEmail.setError(getString(R.string.email_too_long));
                    }

                } else {
                    if (textEmail.getText().toString().equals("")) {
                        textEmail.setError(getString(R.string.field_required));
                    }

                    if (textPassword.getText().toString().equals("")) {
                        textPassword.setError(getString(R.string.field_required));
                    }

                    Toast.makeText(getApplicationContext(), R.string.email_pass_not_entered, Toast.LENGTH_LONG).show();
                }
            }
        });

        btnReset = (Button) findViewById(R.id.btnReset);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textEmail.setText("");
                textPassword.setText("");
            }
        });

        textEmail = (EditText) findViewById(R.id.textEmail);
        textPassword = (EditText) findViewById(R.id.textPassword);


        btnSignUp = (Button) findViewById(R.id.sign_up);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), newAccount.class);
                startActivity(intent);
            }
        });


        int currentVersionCode = 0;
        try {
            currentVersionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (android.content.pm.PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return;
        }

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        int savedVersionCode = prefs.getInt(PREF_VERSION_CODE_KEY, DOESNT_EXIST);

        //I would be able to easily check whether it is a normal run, a new version is installed, or this is the first run
        //if I need
       if (savedVersionCode == DOESNT_EXIST) {
        RelativeLayout.LayoutParams lps = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lps.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        int margin = ((Number) (getResources().getDisplayMetrics().density * 12)).intValue();
        lps.setMargins(margin, margin, margin, margin);

        ViewTarget target = new ViewTarget(R.id.sign_up, this);
        sv = new ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(target)
                .setContentTitle(getString(R.string.lets_get_started))
                .setContentText(getString(R.string.click_to_create_new_acc))
                .setStyle(R.style.CustomShowcaseTheme2)
                .setShowcaseEventListener(this)
                .build();
        sv.setButtonPosition(lps);

        }
        prefs.edit().putInt(PREF_VERSION_CODE_KEY, currentVersionCode).apply();










        callbackManager = CallbackManager.Factory.create();
        login = (LoginButton)findViewById(R.id.login_button);
        profile = (ProfilePictureView)findViewById(R.id.picture);
        shareDialog = new ShareDialog(this);
        share = (Button)findViewById(R.id.share);
        details = (Button)findViewById(R.id.details);
        login.setReadPermissions("public_profile email");
        share.setVisibility(View.INVISIBLE);
        details.setVisibility(View.INVISIBLE);
        details_dialog = new Dialog(this);
        details_dialog.setContentView(R.layout.dialog_details);
        details_dialog.setTitle("Details");
        details_txt = (TextView)details_dialog.findViewById(R.id.details);
        details.setOnClickListener(new View.OnClickListener() {
           @Override
            public void onClick(View view) {
                details_dialog.show();
                }
            });

        if(AccessToken.getCurrentAccessToken() != null){
            share.setVisibility(View.VISIBLE);
            details.setVisibility(View.VISIBLE);
            }
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            if(AccessToken.getCurrentAccessToken() != null) {
            share.setVisibility(View.INVISIBLE);
            details.setVisibility(View.INVISIBLE);
            profile.setProfileId(null);
            }
                System.out.println("here we go");
            }
            });
       share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            ShareLinkContent content = new ShareLinkContent.Builder().build();
            shareDialog.show(content);

            }
            });
        login.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
         @Override
         public void onSuccess(LoginResult loginResult) {

         if(AccessToken.getCurrentAccessToken() != null){
         RequestData();
         share.setVisibility(View.VISIBLE);
         details.setVisibility(View.VISIBLE);
        }
         }
            @Override
           public void onCancel() {
                }

                 @Override
            public void onError(FacebookException exception) {
                }
            });









    }










    public void RequestData(){
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object,GraphResponse response) {

                JSONObject json = response.getJSONObject();
                try {
                    if(json != null){
                        String text = "<b>Name :</b> "+json.getString("name")+"<br><br><b>Email :</b> "+json.getString("email")+"<br><br><b>Profile link :</b> "+json.getString("link");
                        details_txt.setText(Html.fromHtml(text));
                        profile.setProfileId(json.getString("id"));
                        System.out.println(text);
                        facebookEmail = json.getString("email");
                        facebookName = json.getString("name");
                        new facebookLogin().execute();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }



















    private boolean emailNotTooLong()
    {
        String tags = textEmail.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<255 && chars>6;
    }

    private boolean passwordNotTooLong()
    {
        String tags = textPassword.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<128 && chars>6;
    }

    @Override
    protected void onRestart(){
        super.onRestart();
        recreate();
    }


    class GetProductDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(logIn.this);
            pDialog.setMessage(getString(R.string.attepmpt_to_log_in));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;

                    textEmail.setError(null);
                    textPassword.setError(null);

                    try {
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("email", textEmail.getText().toString()));
                        System.out.println("email: " + textEmail.getText().toString());


                        JSONObject json = jsonParser.makeHttpRequest(
                                url_product_details, "GET", params);

                        Log.d("Single Product Details", json.toString());

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {

                            JSONArray productObj = json
                                    .getJSONArray(TAG_PRODUCT);

                            JSONObject product = productObj.getJSONObject(0);


                            hash hash = null;
                            String personalHash = product.getString(TAG_SALT);
                            try {
                                hash = new hash((textPassword.getText().toString()) + ")7sk18zETpOOZvjMR#VkSdRs#1l3gqrWbSl6TE@L" + personalHash);
                            } catch (NoSuchAlgorithmException e) {
                                e.printStackTrace();
                            }
                            String enteredPasswor = hash.getHashedValue();
                            if (enteredPasswor.equals(product.getString(TAG_PRICE))) {

                                    Toast.makeText(getApplicationContext(), R.string.you_are_logged_in, Toast.LENGTH_LONG).show();

                                toLogIn = true;
                                userDetails.setLoggedUserID(Integer.parseInt(product.getString(TAG_ID)));

                                    name = product.getString("name");

                            } else {
                                textPassword.setError(getString(R.string.wrong_pass));
                                textPassword.requestFocus();

                                Toast.makeText(getApplicationContext(), R.string.wrong_pass, Toast.LENGTH_LONG).show();
                            }

                            System.out.println("pass: " + product.getString(TAG_PRICE));


                        } else {
                            textEmail.setError(getString(R.string.incorrect_email));
                            textEmail.requestFocus();

                            Toast.makeText(getApplicationContext(), R.string.incorrect_email, Toast.LENGTH_LONG).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();

            if(toLogIn)
            {
                runOnUiThread(new Runnable() {
                    public void run() {
                        new LoadGroups().execute();
                    }
                });

                toLogIn = false;
            }

        }

    }






    class LoadGroups extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(logIn.this);
            pDialog.setMessage(getString(R.string.loading_tags));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            List<NameValuePair> params = new ArrayList<NameValuePair>();

            System.out.println("userrrr: " + String.valueOf(userDetails.getLoggedUserID()));

            params.add(new BasicNameValuePair("user", String.valueOf(userDetails.getLoggedUserID())));
            JSONObject json = jsonParser.makeHttpRequest(url_load_groups, "GET", params);



            try {
            int success = 0;
                success = json.getInt(TAG_SUCCESS);



            System.out.println("success " + success);
                if (success == 1) {


                        groups = json.getJSONArray("groups");


                    for (int i = 0; i < groups.length(); i++) {

                            JSONObject c = groups.getJSONObject(i);

                            String group = c.getString("group_chosen");
                            userGroups.add(group);

                            String addTasks = c.getString("add_tasks");
                            addTasksPermission.add(Integer.valueOf(addTasks));

                            String manageTags = c.getString("manage_tags");
                            manageTagsPermission.add(Integer.valueOf(manageTags));

                            String manageUsers = c.getString("manage_group_users");
                            manageGroupUsersPermission.add(Integer.valueOf(manageUsers));


                    }

                    userDetails.setUserLoggedIn(true);
                    System.out.println("nameee:   " + name);

                    userDetails.setLoggedUserName(name);
                    if(!textEmail.getText().toString().equals("")) {
                        com.radoslav.socialreminder.userDetails.setLoggedEmail(textEmail.getText().toString());
                    }
                    else{
                        com.radoslav.socialreminder.userDetails.setLoggedEmail(facebookEmail);
                    }
                    System.out.println("eventually, what's your email " + com.radoslav.socialreminder.userDetails.loggedEmail);

                    userDetails.setCurrentGroupChosenID(Integer.parseInt(userGroups.get(0)));
                    com.radoslav.socialreminder.userDetails.setGroups(userGroups);
                    com.radoslav.socialreminder.userDetails.setAddTasksPermission(addTasksPermission);
                    com.radoslav.socialreminder.userDetails.setManageTagsPermission(manageTagsPermission);
                    com.radoslav.socialreminder.userDetails.setManageGroupUsersPermission(manageGroupUsersPermission);


                    Intent intent = new Intent();
                    intent.putExtra("email", com.radoslav.socialreminder.userDetails.loggedEmail);
                    intent.putExtra("name", name);
                    setResult(RESULT_OK, intent);
                    finish();




                } else if (userGroups.size()==0){

                    Intent intent = new Intent(logIn.this, allGroups.class);
                    userDetails.setLoggedUserName(name);

                    startActivityForResult(intent, 6);
                }
                System.out.println("size " + userGroups.size());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();


        }

    }





    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if((requestCode == 6) && (resultCode == 100))
        {
            AlertDialog.Builder buildDialog = new AlertDialog.Builder(logIn.this);
            buildDialog.setMessage(R.string.membership_sent);
            buildDialog.setCancelable(false);
            buildDialog.setPositiveButton(R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            LoginManager.getInstance().logOut();
                            dialog.cancel();
                            recreate();
                        }
                    });


            AlertDialog showTheDialog = buildDialog.create();
            showTheDialog.show();
        }

    }

    @Override
    public void onBackPressed() {
        clicks++;
        Toast toast = Toast.makeText(getApplicationContext(), R.string.click_once_again, Toast.LENGTH_LONG);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        if( v != null) v.setGravity(Gravity.CENTER);
        toast.show();

        if (clicks == 2) {
            System.out.println("trying to exit the app");

            setResult(720);

            finish();

        }

        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                System.out.println("Seconds remaining: " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                System.out.println("Done");
                System.out.println("zero now");
                clicks = 0;
            }

        }.start();



    }






    class facebookLogin extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(logIn.this);
            pDialog.setMessage(getString(R.string.attepmpt_to_log_in));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... params) {


                    int success;


                    try {
                        List<NameValuePair> parameters = new ArrayList<>();
                        parameters.add(new BasicNameValuePair("email", facebookEmail));

                        System.out.println("what we are trying to find: " + facebookEmail);

                        JSONObject json = jsonParser.makeHttpRequest(
                                url_product_details, "GET", parameters);

                        Log.d("Single Product Details", json.toString());

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {

                            JSONArray productObj = json
                                    .getJSONArray(TAG_PRODUCT);

                            JSONObject product = productObj.getJSONObject(0);

                                toLogIn = true;
                                userDetails.setLoggedUserID(Integer.parseInt(product.getString(TAG_ID)));

                                name = product.getString("name");


                        } else {

                                System.out.println("gotta create");
                                accountExist = false;



                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }



            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();

            if(toLogIn)
            {

                new LoadGroups().execute();
                toLogIn = false;
            }

            if(!accountExist)
            {

                new createNewUserFacebook().execute();
                accountExist = true;
            }

        }

    }


















    @Override
    public void onClick(View view) {

        int viewId = view.getId();
        switch (viewId) {
            case R.id.btnLogIn:
                if (sv.isShown()) {
                    sv.setStyle(R.style.CustomShowcaseTheme);
                } else {
                    sv.show();
                }
                break;
        }
    }

    @Override
    public void onShowcaseViewHide(ShowcaseView showcaseView) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
           listView.setAlpha(1f);
        }
    }

    @Override
    public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
    }

    @Override
    public void onShowcaseViewShow(ShowcaseView showcaseView) {

    }


    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

    }


    private static class HardcodedListAdapter extends ArrayAdapter {


        public HardcodedListAdapter(Context context) {
            super(context, R.layout.activity_log_in);
        }



        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.activity_log_in, parent, false);
            }

            return convertView;
        }
    }



















    class createNewUserFacebook extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(logIn.this);
            dialog.setMessage(getString(R.string.creating_user));
            dialog.setIndeterminate(false);
            dialog.setCancelable(true);
            dialog.show();
        }


        protected String doInBackground(String... args) {
            hash hash = null;

System.out.println("creating");
           /* final Random r = new SecureRandom();
            byte[] salt = new byte[32];
            r.nextBytes(salt);*/
            //String encodedSalt = Base64.encodeToString(salt, 32);
            String encodedSalt = "facebook";

            try {
                hash = new hash(facebookName + ")7sk18zETpOOZvjMR#VkSdRs#1l3gqrWbSl6TE@L" + encodedSalt);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            String name = facebookEmail;
            String price = hash.getHashedValue();;
            String description = facebookName;


                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("name", name));
                params.add(new BasicNameValuePair("password", price));
                params.add(new BasicNameValuePair("description", description));
                params.add(new BasicNameValuePair("salt", encodedSalt));


                JSONObject json = jsonParser.makeHttpRequest(url_create_product,
                        "POST", params);

                Log.d("Create Response", json.toString());

                try {
                    int success = json.getInt(TAG_SUCCESS);

                   /* if (success == 1) {



                    } else {



                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            return null;
        }


        protected void onPostExecute(String file_url) {
            dialog.dismiss();
            new facebookLogin().execute();
        }

    }





}
