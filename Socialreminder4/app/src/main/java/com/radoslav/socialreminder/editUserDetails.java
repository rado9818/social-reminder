package com.radoslav.socialreminder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class editUserDetails extends Activity {

    EditText txtName;
    EditText txtPrice;
    EditText txtDesc;
    EditText txtCreatedAt;
    Button btnSave;
    Button btnDelete;

    String pid;

    private String firstNewPass, secondNewPass;

    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();

    private static final String url_product_detials = "http://radoslavbonev.net/get_user_details.php";

    private static final String url_update_product = "http://radoslavbonev.net/update_user.php";

    private static final String url_delete_user = "http://radoslavbonev.net/delete_user.php";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_PRODUCT = "product";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";
    private static final String TAG_PRICE = "password";
    private static final String TAG_DESCRIPTION = "description";
    private static final String TAG_SALT = "salt";

    private String personalSalt;

    private String currentPassword;

    private Boolean passwordsMatch = true;
    private Boolean oldPasswordCorrect = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_details);

        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);


        pid = String.valueOf(userDetails.loggedUserID);

        new GetProductDetails().execute();

        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                txtName.setError(null);
                txtPrice.setError(null);
                txtDesc.setError(null);
                if(emailNotTooLong())
                {
                    if(passwordNotTooLong()) {


                        if(nameNotTooLong()) {
                            new SaveProductDetails().execute();
                        }
                        else
                        {
                            txtDesc.setError(getString(R.string.name_too_long));
                        }


                    }
                    else
                    {
                        txtPrice.setError(getString(R.string.password_too_long));
                    }

                }
                else
                {
                    txtName.setError(getString(R.string.email_too_long));
                }


                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(!oldPasswordCorrect)
                {
                    System.out.println("old pass is not correct");
                    Toast.makeText(editUserDetails.this, R.string.old_pass_not_correct, Toast.LENGTH_LONG).show();
                }
                else if(!passwordsMatch)
                {
                    System.out.println("passwords do not match");
                    Toast.makeText(getApplicationContext(), R.string.passwords_dont_match, Toast.LENGTH_LONG).show();

                }
                oldPasswordCorrect = true;
                passwordsMatch = true;



            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                new DeleteProduct().execute();
            }
        });

    }

    private boolean emailNotTooLong()
    {
        String tags = txtName.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<255 && chars>6;
    }

    private boolean passwordNotTooLong()
    {
        String tags = txtPrice.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<128 && chars>6;
    }

    private boolean nameNotTooLong()
    {
        String tags = txtDesc.getText().toString();
        int chars = tags.codePointCount(0, tags.length());

        return chars<128 && chars>2;
    }
    class GetProductDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editUserDetails.this);
            pDialog.setMessage(getString(R.string.loading_users_details));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... params) {

            runOnUiThread(new Runnable() {
                public void run() {
                    int success;
                    try {
                        List<NameValuePair> params = new ArrayList<NameValuePair>();
                        params.add(new BasicNameValuePair("pid", pid));


                        JSONObject json = jsonParser.makeHttpRequest(
                                url_product_detials, "GET", params);

                        Log.d("Single Product Details", json.toString());

                        success = json.getInt(TAG_SUCCESS);
                        if (success == 1) {
                            JSONArray productObj = json
                                    .getJSONArray(TAG_PRODUCT);

                            JSONObject product = productObj.getJSONObject(0);


                            txtName = (EditText) findViewById(R.id.inputName);
                            txtPrice = (EditText) findViewById(R.id.inputPrice);
                            txtDesc = (EditText) findViewById(R.id.inputDesc);

                            txtName.setText(product.getString(TAG_NAME));
                            currentPassword = product.getString(TAG_PRICE);
                            txtDesc.setText(product.getString(TAG_DESCRIPTION));
                            personalSalt = product.getString(TAG_SALT);

                        }else{
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }



        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }


    class SaveProductDetails extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editUserDetails.this);
            pDialog.setMessage(getString(R.string.save_user));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {
            TextView oldPassword = (TextView) findViewById(R.id.oldPassword);

            hash hash = null;
            try {
                hash = new hash((oldPassword.getText().toString()) + ")7sk18zETpOOZvjMR#VkSdRs#1l3gqrWbSl6TE@L" + personalSalt);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            String name = txtName.getText().toString();
            String description = txtDesc.getText().toString();



            String inputOldPass = hash.getHashedValue();

            TextView repeatPassword = (TextView) findViewById(R.id.repeatPassword);


            firstNewPass = txtPrice.getText().toString();
            secondNewPass = repeatPassword.getText().toString();

            System.out.println("old pass:   " + inputOldPass);
            System.out.println("current pass:   " + currentPassword);
            System.out.println("-------------");
            System.out.println("new pass 1:   "+ firstNewPass);
            System.out.println("new pass 2:   "+ secondNewPass);


            if((inputOldPass.equals(currentPassword)) && (firstNewPass.equals(secondNewPass))    )
            {

                com.radoslav.socialreminder.hash newHash = null;
                try {
                    newHash = new hash((txtPrice.getText().toString()) + ")7sk18zETpOOZvjMR#VkSdRs#1l3gqrWbSl6TE@L" + personalSalt);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }

                String newPassword = newHash.getHashedValue();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair(TAG_PID, pid));
            params.add(new BasicNameValuePair(TAG_NAME, name));
            params.add(new BasicNameValuePair(TAG_PRICE, newPassword));
            params.add(new BasicNameValuePair(TAG_DESCRIPTION, description));


            JSONObject json = jsonParser.makeHttpRequest(url_update_product,
                    "POST", params);

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    Intent i = getIntent();

                    setResult(100, i);
                    finish();
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            }
            else
            {
                if(!inputOldPass.equals(currentPassword))
                {
                    oldPasswordCorrect = false;
                    System.out.println("at oldPassCorrect to false:   " + oldPasswordCorrect);

                }
                else if(!firstNewPass.equals(secondNewPass))
                {
                    passwordsMatch = false;
                    System.out.println("at passwordsMatch to false:   " + passwordsMatch);

                }

            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();

        }
    }


    class DeleteProduct extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(editUserDetails.this);
            pDialog.setMessage(getString(R.string.deleting_user));
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }


        protected String doInBackground(String... args) {

            int success;
            try {

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("pid", pid));

                JSONObject json = jsonParser.makeHttpRequest(
                        url_delete_user, "POST", params);

                Log.d("Delete User", json.toString());

                success = json.getInt(TAG_SUCCESS);
                if (success == 1) {

                    Intent i = getIntent();

                    userDetails userDetails = new userDetails();

                    userDetails.setCurrentGroupChosenID(0);
                    userDetails.setUserLoggedIn(false);
                    userDetails.setLoggedUserID(0);

                    setResult(100, i);
                    finish();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }


        protected void onPostExecute(String file_url) {
            pDialog.dismiss();

        }

    }
}
