package com.radoslav.socialreminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Rado on 17/07/2015.
 **/
public class DBOperations extends SQLiteOpenHelper {

    public static final int DBVersion = 1;
    public String query = "CREATE TABLE " + tableData.TableInfo.TABLE_NAME +"(" + tableData.TableInfo.TASK_ID + " INTEGER PRIMARY KEY, " + tableData.TableInfo.TASK_CONTENT + " TEXT, " + tableData.TableInfo.TASK_DATE + " TEXT, " + tableData.TableInfo.TASK_TAGS + " TEXT, " + tableData.TableInfo.TASK_GROUP + " TEXT" + ");";


    public DBOperations(Context context) {
        super(context, tableData.TableInfo.DATABASE_NAME, null, DBVersion);
        Log.d("DBOperations", "DB was created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(query);
        Log.d("DBOperations", "Table was created");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void putInformation(DBOperations dbOperations, String content, String date, String tags, String group)
    {
        SQLiteDatabase SQ = dbOperations.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(tableData.TableInfo.TASK_CONTENT, content);
        contentValues.put(tableData.TableInfo.TASK_DATE, date);
        contentValues.put(tableData.TableInfo.TASK_TAGS, tags);
        contentValues.put(tableData.TableInfo.TASK_GROUP, group);
        SQ.insert(tableData.TableInfo.TABLE_NAME, null, contentValues);
        Log.d("DBOperations", "Inserted a row");

    }

    public void deleteInformation(DBOperations dbOperations, int currentID)
    {
        String selection = tableData.TableInfo.TASK_ID + " LIKE ?";
        String[] args = {String.valueOf(currentID)};
        SQLiteDatabase SQ = dbOperations.getWritableDatabase();
        SQ.delete(tableData.TableInfo.TABLE_NAME, selection, args);
        Log.d("DBOperations", "Deleted");

    }

    public void updateInfo(DBOperations dbOperations, int currentID, String newMyContent, String newMyDate)
    {
        SQLiteDatabase SQ = dbOperations.getWritableDatabase();
        String selection = tableData.TableInfo.TASK_ID + " LIKE ?";
        String args[] = {String.valueOf(currentID)};
        ContentValues values = new ContentValues();
        values.put(tableData.TableInfo.TASK_CONTENT, newMyContent);
        values.put(tableData.TableInfo.TASK_DATE, newMyDate);
        SQ.update(tableData.TableInfo.TABLE_NAME, values, selection, args);
    }

    public Cursor getInformation(DBOperations dbOperations)
    {
        SQLiteDatabase SQ = dbOperations.getReadableDatabase();
        String[] columns = {tableData.TableInfo.TASK_ID, tableData.TableInfo.TASK_CONTENT, tableData.TableInfo.TASK_DATE, tableData.TableInfo.TASK_TAGS};
        System.out.println("DB: " + String.valueOf(userDetails.currentGroupChosenID));
        Cursor cursor = SQ.query(tableData.TableInfo.TABLE_NAME, columns, tableData.TableInfo.TASK_GROUP + " = ? ", new String[] {String.valueOf(userDetails.currentGroupChosenID)}, null, null, null);
        return cursor;
    }

}
